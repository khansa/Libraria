<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Blog;
use App\Models\Books;
use App\Models\Community;
use App\Models\News;
use App\Models\Comments;


use DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create();
        Blog::factory()->count(10)->create();
        Books::factory()->count(50)->create();
        Community::factory()->count(10)->create();
        News::factory()->count(10)->create();
        Comments::factory()->count(10)->create();




$categories = ['Action','History','Fantasy','Historical','Horror','Thriller','Mystery','Non-Fiction','Romance','Fiction','Science'];


$id = 0;
        foreach ($categories as $categorie){ 
            $id ++;
            DB::table('book_categories')->insert(
                array(
                    'id' =>   $id    ,

                       'category'   =>   $categorie
                )
           );
            }

            $formats = ['pdf','Books','eAudio','Videos','Magazine','Kids','Teens','DVDS'];

            $id = 0;
            foreach ($formats as $format){ 
                $id ++;
                DB::table('format_books')->insert(
                    array(
                        'id' =>   $id    ,
    
                           'format'   =>   $format
                    )
               );
                }

    }
}
