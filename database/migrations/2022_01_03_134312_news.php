<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class News extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->string('img');

            $table->longText('content');
            $table->string('author')->default('Anonymous');
            $table->longText('title');
            $table->longText('contact_info');

            $table->integer('views')->default(0);

            $table->string('location');
            $table->string('start_time');
            $table->string('end_time');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('news');

    }
}
