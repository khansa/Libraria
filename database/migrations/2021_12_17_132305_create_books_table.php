<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('title', 1000);
            $table->string('author', 1000);
            $table->string('rating', 1000);
            $table->string('edition', 1000);
            $table->string('img', 1000);

            $table->text('description');
            $table->integer('category_id');
            $table->integer('length');
            $table->string('format');
            $table->string('language');
            $table->string('url')->default('');

            $table->string('publisher');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
