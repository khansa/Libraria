<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CommentsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [


            

          

            'content' => $this->faker->text(9000),
             'img_author' => $this->faker->randomElement(['emma','mathew','peter']).'.jpg',
    
            'blog_id' => $this->faker->numberBetween(1,10) ,
'author' => $this->faker->name,

'subject' => $this->faker->title,

            
        ];
    }
}
