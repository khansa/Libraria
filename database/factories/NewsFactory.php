<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class NewsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [






 
            'title' => $this->faker->title,
          
            'contact_info' => $this->faker->title,

            'content' => $this->faker->text(100),

            'img' => 'news-listing-0' . $this->faker->numberBetween(1, 4).'.jpg',

'author' => $this->faker->name,


'title' =>$this->faker->title,
'likes' => 0,
'views' => 0,
'comments' => 0,
'contact_info' => $this->faker->text(100),

'location' => 'Tunisie',
'start_time' => $this->faker->time,
'end_time' => $this->faker->time,


            //
        ];
    }
}
