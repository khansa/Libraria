<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BooksFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {


        $types_format = ['eBooks','eAudio','Videos','Magazine','Kids','Teens','DVDS'];

        
        return [
            

            'title' => $this->faker->title,
          
            'edition' => $this->faker->title,

            'description' => $this->faker->text(100),
            'category_id' => $this->faker->numberBetween(1, 11),

            'img' => 'book-media-0' . $this->faker->numberBetween(1, 6).'.jpg',

'author' => $this->faker->name,


'rating' => $this->faker->numberBetween(0,5),


'length' => $this->faker->numberBetween(0, 900),

/* 'format' => $this->faker->randomElement( $types_format ), */

 'format' => 'pdf' , 

'language' => $this->faker->word,
'publisher' => $this->faker->word,

        ];
    }
}
