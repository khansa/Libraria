<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CommunityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            

            'content' => $this->faker->text(100),
          


            'img' => 'testimonial-image-0' . $this->faker->numberBetween(1, 2).'.jpg',

'author' => $this->faker->name,






        ];
    }
}
