<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Blog;
class BlogFactory extends Factory
{


    protected $model = Blog::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [

            'title' => $this->faker->title,


            'content' => $this->faker->text(1000),
            'author_description' => $this->faker->text(100),

             'img' => 'blog-0' . $this->faker->randomDigitNot(0).'.jpg',
            'theme' => $this->faker->word, // 
            'likes' => 0,
            'views' => 0,
'author' => $this->faker->name,


        ];
    }
}
