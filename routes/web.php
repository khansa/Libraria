<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\BooksController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\HelperController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\AdminController;

use App\Http\Controllers\DatatablesController;
use App\Models\Book_categories;
use App\Models\Books;
use App\Models\Community;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/








Route::get('/', function () {
    return view('library.index',['categories' =>  Book_categories::all(),'data_community' => Community::all()]);
})->name('index');





/* Blog */
Route::get('/blog', [BlogController::class, 'show_all_blogs'])->name('blog');



Route::get('/blog-detail/{num}', [BlogController::class, 'blog_detail'])->name('blog-detail');



Route::post('/add_blog_comment/{num}', [BlogController::class, 'add_blog_comment'])->name('add_blog_comment');



/* Blog */



/* Book */
Route::get('/books-media-detail-v1/{num}', [BooksController::class, 'books_media_detail_show'])->name('books-media-detail-v1');






Route::get('/books-media-list-view', [BooksController::class, 'books_media_list_show'])->name('books-media-list-view');


Route::get('/showbook/{id}', [BooksController::class, 'showbook'])->name('showbook');


Route::get('/downloadbook/{id}/{title}', [BooksController::class, 'downloadbook'])->name('downloadbook');

/* Book */



/* News */





Route::get('/news-events-detail/{num}', [NewsController::class, 'news_detail'])->name('news-events-detail');

Route::get('/news-events-list-view', [NewsController::class, 'news_list'])->name('news-events-list-view');




/* News */


Route::get('/services', function () {
    return view('library.services',['data_community' => Community::all()]);
})->name('services');




Route::get( '/contact', [EmailController::class, 'contact'])->name('contact');


Route::post( '/contact', [EmailController::class, 'contact_send'])->name('contact');

Route::post('/newsletter', [HelperController::class, 'newsletter_show'])->name('newsletter');




Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');




Route::get('/disconnect', function () {
    Auth::logout();


    return redirect('/');
})->name('disconnect');


Route::middleware(['CheckAdmin'])->prefix('admin')->group(function () {


    Route::get('/', [AdminController::class, 'show_index_admin'])->name('show_index_admin');



Route::get('/crudbooks', [AdminController::class, 'show_crudbook'])->name('crudBooks');



Route::get('/crudnews', [AdminController::class, 'show_crudnews'])->name('crudNews');



Route::get('/cruduser', [AdminController::class, 'show_cruduser'])->name('crudUser');


Route::get('/crudblog', [AdminController::class, 'show_crudblog'])->name('crudBlog');






Route::post('/datatables/data', [DatatablesController::class, 'anyData'])->name('data');





Route::match(['get','post' ],'/crud/add/{model}', [AdminController::class, 'crudadd'])->name('crudadd');

Route::get('/crud/delete/{model}/{id}', [AdminController::class, 'cruddelete'])->name('cruddelete');

Route::match(['get','post' ],'/crud/edit/{model}/{id}', [AdminController::class, 'crudedit'])->name('crudedit');


});

/* Videos */
Route::get('/video', function () {
    if (Auth::check())
  {  return view('library.video');}
  else
  {
    return redirect()->guest('/login');  
  }
})->name('video');

