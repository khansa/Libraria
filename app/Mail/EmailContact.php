<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailContact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($first_name,$last_name,$phone,$email,$messages)
    {
       $this->email = $email;
       $this->last_name = $last_name;

       $this->first_name = $first_name;

       $this->phone = $phone;

       $this->messages = $messages;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('contactmail')->with([
            'email' =>  $this->email,
            'last_name' =>  $this->last_name,
            'first_name' =>  $this->first_name,
            'phone' =>  $this->phone,
            'messages' =>  (string) $this->messages

        ])->subject('Contact Formulaire');
    }
}
