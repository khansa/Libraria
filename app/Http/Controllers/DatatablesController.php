<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Yajra\Datatables\Datatables;

class DatatablesController extends Controller
{
    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */


    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData(Request $request)
    {



        $model_name = 'App\\Models\\' . $_POST["data"];
        $model = $model_name::query()->orderByDesc('id');
        $data =  DataTables::eloquent($model)

            ->addColumn(
                'editer',
                '<a href="' .
                    url('admin/crud/edit/{{$_POST["data"]}}/{{$id}}')  .




                    '"class="colorlinkedit">  <input type="button" value="Editer" > </a>'
            )

            ->addColumn(
                'supprimer',
                '<a href="#"
                     




                    class="colorlinkdelete">  <input type="button" href="'. 
                    
                    url('admin/crud/delete/{{$_POST["data"]}}/{{$id}}') .
                    
                    
                    
                    
                   '" value="Supprimer" onclick="submitResult(event)"> </a>'
            )
            ->rawColumns([ 'editer', 'supprimer'])

            ->toJson();

        return $data;
    }
}
