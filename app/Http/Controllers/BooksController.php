<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book_categories;
use App\Models\Books;
use Illuminate\Support\Facades\Storage;
use File;
use DB;
class BooksController extends Controller
{
    //
    function books_media_detail_show($num,Request $request)
    {

         $book_item_value = Books::where('id','=',$num)->firstorFail();
        return view('library.books-media-detail-v1',['categories' =>  Book_categories::all(),'book_item_value' => $book_item_value]);

    }



    function books_media_list_show(Request $request)
    { 
        
 
        /* no filter all books*/ 
        if ( sizeof($_GET) == 0 || ($request->input('category') =='all' &&  $request->input('keywords') ==""))
{     
     
    $books = Books::all();

        return view('library.books-media-list-view',['categories' =>  Book_categories::all(),'books' =>     $books]);

 }
    
    if ($request->input('keywords') != "" && $request->input('category') =='all')
    {   
        $search_books = Books::where('title','like', '%' .$request->input('keywords') . '%' )->get();
        return view('library.books-media-list-view',['categories' =>  Book_categories::all(),'books' => $search_books ]);

    }
 /* no filter all books with search*/

    if ($request->input('keywords') == null && $request->input('category') !='all')
    {   
        $category = Book_categories::where('category','=',$_GET['category'])->firstorFail();
        $search_books = Books::where('category_id','=',$category->id)->get();
        return view('library.books-media-list-view',['categories' =>  Book_categories::all(),'books' => $search_books ]);

    }


if (sizeof($_GET) > 0 )
{
    {    
        
        $category = Book_categories::where('category','=',$_GET['category'])->firstorFail();

       if ($request->input('keywords') =="")
       {        $search_books = Books::where('category_id','=',$category->id)->get();

    }
    else 
    { 
        $search_books = Books::where('category_id','=',$category->id)->where('title','like', '%' .$request->input('keywords') . '%' )->get();

    
    return view('library.books-media-list-view',['categories' =>  Book_categories::all(),'books' => $search_books]);

    }

}
    }



}


function showbook($id)
{

    if (Storage::exists("public/$id"))
    {
        return Storage::response("public/$id");

    }
    abort(404);

}


function downloadbook($id,$title)
{ 
  if (Storage::exists("public/$id"))
  {
 
    return  Storage::download("public/$id","$title.pdf" );

  }

  abort(404);


}



}