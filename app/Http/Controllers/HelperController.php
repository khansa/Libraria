<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Newsletters;
use Auth;

use App\Models\Book_categories;
use App\Models\Books;
use App\Models\Community;
class HelperController extends Controller
{
    //


    function newsletter_show(Request $request)
    { 
        if ($request->newsletter == null)
{
    abort (404);
}        
        
        if (Auth::check())
        { 
if (!Newsletters::where('email','=',$request->newsletter)->first())
{
    $newsletter = Newsletters::create([
        'email' => $request->newsletter
    ]);



    return view('library.index',['categories' =>  Book_categories::all(),'data_community' => Community::all()])->with('successMsg','Votre email est ajouté au newsletter .');
}
else
{
    return view('library.index',['categories' =>  Book_categories::all(),'data_community' => Community::all()])->with('errorMsg','Votre email est déja existant .');
}

    }
    else 
    abort(404);
}}
