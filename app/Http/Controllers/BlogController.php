<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Comments;
use Auth;
use DB;
class BlogController extends Controller
{
    //


    public function show_all_blogs(Request $request)
    {
        return view('library.blog',['all_blog_data' => Blog::all()]);

    }


    public function add_blog_comment (Request $request,$num)
    {
        if(!Auth::user())

{
    return 404;
}
else 
{
DB::insert("insert into comments (content,author,blog_id ,subject,img_author,created_at,updated_at) values (?, ?, ?, ?,?,?,?)", [
    
    $request->comment,
    Auth::user()->name,
    $num ,
    $request->subject,
'',
date("Y-m-d H:i:s"),
date("Y-m-d H:i:s")


]

);


return redirect()->action([BlogController::class, 'blog_detail'],['num' => $num,]);


}


    }


    public function blog_detail(Request $request,$num)
    { $blog_item_value = Blog::where('id','=',$num)->firstorFail();


        $blog_item_value->views++ ;

        $blog_item_value->save();

        $comments = Comments::where('blog_id','=',$num)->get();

        return view('library.blog-detail',['blog_item_value'=> $blog_item_value,'comments' => $comments]);


    }
}
