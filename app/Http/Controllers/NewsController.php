<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;
class NewsController extends Controller
{
    //
    function news_list()
    {

        return view('library.news-events-list-view',['all_news' => News::all()]);

    }


    function news_detail($num)
    {

        $news_item = News::where('id','=',$num)->firstorFail();


        $news_item->views++ ;

        $news_item->save();
        return view('library.news-events-detail',['news_item' => $news_item]);

    }
}
