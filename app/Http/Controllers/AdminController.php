<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Yajra\Datatables\Datatables;
use DB;
use Hash;
use Arr;
class AdminController extends Controller
{
    //


    function show_index_admin()
    {



        return view('admin');
    }




    function show_crudbook()
    {

        return view('CRUD.show_crudBooks');
    }



    function show_crudnews()
    {
        return view('CRUD.show_crudNews');
    }


    function show_cruduser()
    {
        return view('CRUD.show_crudUser');
    }


    function show_crudblog()
    {
        return view('CRUD.show_crudBlog');
    }






    public function crudadd($model,Request $request)
    { if 
        ($request->method() =="GET")
        {   return view ("CRUD.add_$model");
    }


  if  ($request->method() =="POST")
    {
        $array = $request->all();

        /* Books */
        
        if ($model == "Books")
        {


           
    


        if ($request->hasFile('img')) {
            $uploadedFile_img = $request->file('img');
           
            $uploadedFile_img->move(base_path('\public\images\books-media\list-view'),$uploadedFile_img->getClientOriginalName());


        }

        if ($request->hasFile('url')) {
            $uploadedFile_url = $request->file('url');
            $element = DB::table($model)->orderBy('id','desc')->first();
$extension = $uploadedFile_url->getClientOriginalExtension();

$elementname=  $element->id + 1;
$final_name ="$elementname.$extension";
            $uploadedFile_url->move(base_path('\public\storage'),$final_name );

  
/*             dd($uploadedFile_url);
 */
        }

        $new_ele = $element->id + 1;


        $array['id'] = $new_ele;

        Arr::set($array , 'img', $uploadedFile_img->getClientOriginalName());
        Arr::set($array , 'url', $final_name);


    }


      /* Books */



      if ($model == "News")
      {
        if ($request->hasFile('img')) {
            $uploadedFile_img = $request->file('img');
           $name = "news-event-".mt_rand(1000000, 9999999). ".".  $uploadedFile_img->getClientOriginalExtension();
         
           $uploadedFile_img->move(base_path('\public\images\news-event'),$name );
           Arr::set($array , 'img', $name);

        

        }

      }




      if ($model == "Blog")
      {

        if ($request->hasFile('img')) {
            $uploadedFile_img = $request->file('img');
           $name = "blog-".mt_rand(1000000, 9999999). ".".  $uploadedFile_img->getClientOriginalExtension();
         
           $uploadedFile_img->move(base_path('\public\images\blog'),$name );
           Arr::set($array , 'img', $name);

        

        }
      }


/* User */
        if ($model == "User")
{

  
    $password = Hash::make($request->password);

    Arr::set($array , 'password', $password);

    DB::table('users')->insert($array);
    return view ('CRUD.show_crudUser');

}
else
{
        DB::table($model)->insert($array);

        return view ("CRUD.show_crud$model");

    }


    /* User */
}
    }


    public function cruddelete($model,$id)
    {
        $model_final = 'App\\Models\\' . $model;
        $data = $model_final::find($id);
    
$data->delete();

return view("CRUD.show_crud$model" );

    }


    public function crudedit($model,$id,Request $request)
    { 

        $model_final     = 'App\\Models\\' . $model;


        if 
        ($request->method() =="GET")
        {   


            


        $data  = $model_final::find($id);
        
        
        
                return view ("CRUD.edit_$model",['id' => $id,'data' => $data]);
    }


  if  ($request->method() =="POST")
    {  
        $array = $request->all();
       
        /* Books */
        
        if ($model == "Books")
        {
        if ($request->hasFile('img')) {
            $uploadedFile_img = $request->file('img');
           
            $uploadedFile_img->move(base_path('\public\images\books-media\list-view'),$uploadedFile_img->getClientOriginalName());


        }

        if ($request->hasFile('url')) {
            $uploadedFile_url = $request->file('url');
            $element = DB::table($model)->orderBy('id','desc')->first();
$extension = $uploadedFile_url->getClientOriginalExtension();

$elementname=  $element->id + 1;
$final_name ="$elementname.$extension";
            $uploadedFile_url->move(base_path('\public\storage'),$final_name );

  
/*             dd($uploadedFile_url);
 */
        }

        Arr::set($array , 'img', $uploadedFile_img->getClientOriginalName());
       
        Arr::set($array , 'url', $final_name);

        

        DB::table($model)
->where('id', $id)
->update(  $array );



    }


      /* Books */



      if ($model == "News")
      {
        if ($request->hasFile('img')) {
            $uploadedFile_img = $request->file('img');
           $name = "news-event-".mt_rand(1000000, 9999999). ".".  $uploadedFile_img->getClientOriginalExtension();
         
           $uploadedFile_img->move(base_path('\public\images\news-event'),$name );
           Arr::set($array , 'img', $name);

           DB::table($model)
           ->where('id', $id)
           ->update(  $array );

        }

      }




      if ($model == "Blog")
      {
        if ($request->hasFile('img')) {
            $uploadedFile_img = $request->file('img');
           $name = "blog-".mt_rand(1000000, 9999999). ".".  $uploadedFile_img->getClientOriginalExtension();
         
           $uploadedFile_img->move(base_path('\public\images\blog'),$name );
           Arr::set($array , 'img', $name);

DB::table($model)
->where('id', $id)
->update(  $array );



        }
      }





        if ($model == "User")
{


    $password = Hash::make($request->password);

        Arr::set($array , 'password', $password);


        DB::table('users')
        ->where('id', $id)
        ->update(  $array );}

        
       
        
   
        return view ("CRUD.show_crud$model");
}



}

}
