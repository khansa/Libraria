<?php 

use App\Models\Book_categories;
use App\Models\Books;
use Illuminate\Support\Facades\Storage;
 function count_numberofbook($category)
{

    return DB::table('books')->where('format','=',  $category )->count();



}



function get_category_books($category_id)
{
   $books =   Books::where('category_id','=',$category_id)->get();
if ( $books!= null)

    return  $books ;

    else 
    return null;



}


function randomcolor()
{

  $items = ['blue','red','purple','pink','green','pink','orange'];


return $items[array_rand($items)];

}



function get_size_file_book($id)
{


  if (Storage::exists("public/$id"))
  {
  $size =   Storage::size("public/$id");

  $precision = 2;
  if ($size > 0) {
    $size = (int) $size;
    $base = log($size) / log(1024);
    $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');

    return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
} else {
    return $size;
}

  }
  return  "Fichier introuvable";

}


function check_if_file_exist($id)
{
  if (Storage::exists("public/$id"))
  {
    return true;
  }
else return false;
}