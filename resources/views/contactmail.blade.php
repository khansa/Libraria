<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Email Contact</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.dark\:text-gray-500{--tw-text-opacity:1;color:#6b7280;color:rgba(107,114,128,var(--tw-text-opacity))}}
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
       
     




            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                <div class="flex justify-center pt-8 sm:justify-start sm:pt-0">
                <img src="{{url('images/libraria-logo-v1.png')}}" alt="LIBRARIA">
 
                </div>

<br><br>

                <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                <div class="flex justify-center pt-8 sm:justify-start sm:pt-0">
            <h2>    Informations du contact : </h2>
                </div>

                <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
                    <div class="grid grid-cols-1 md:grid-cols-2">
                        <div class="p-6">
                            <div class="flex items-center">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAABpElEQVRoge2ZO07EMBCGvwS0iA5BjcQtkOAOewg62oUGLZQQOAoNt0DiCkgsLxEKCpotKEOR1+4qj3HIOAnyJ7mIlXjmtz32xAaHw+FwdMQIuAE+gajlEgJBYkOdawUBq+XKhpAwMXag0PYh+ciok/baUNsHwNc2sIDGlM1i0CPvLU9RgDZBasiGMQ2yGLQxItpEYDdGVHFC+oaJkDXgHHgGfoB7YF/DqaZIV60JxWv5RM81EZn/UiEPFAt5VHJQSgREJlNrq6R+swVn/oyJkNeS+i/h922lJYWYCPkuqZ8ZtKHGusG7eyX1u8Lv1TMHabCPKR7qsZ5rIoxXLY94D1kU8UL3OZqxEIAjloUcNzDYdrAbL78A85rnTpGOyAi4Zbl37oANPddEiKfWNnACPFE81DPgFNhRdLaKWiE+MCWePpK5OwfOsJ9R1wq5QCZgtUxrDKoEe5WQ94aG3roQ4v7Z+4YT0jeckL7xb4RIfqzK0hfbh96V9nziKzeID4SHRupzCPGRvMmO2tYO3aa9S4jT84D8Cm5IQj4SEVYuWx0ORw/4BVdkfPTcy1fHAAAAAElFTkSuQmCC"/>
                                <div class="ml-4 text-lg leading-7 font-semibold"><a  class="underline text-gray-900 dark:text-white"> Nom : </a></div>
                            </div>

                            <div class="ml-12">
                                
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                   {{ $first_name }}
                                </div>
                            </div>
                        </div>

                        <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-t-0 md:border-l">
                            <div class="flex items-center">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAABpElEQVRoge2ZO07EMBCGvwS0iA5BjcQtkOAOewg62oUGLZQQOAoNt0DiCkgsLxEKCpotKEOR1+4qj3HIOAnyJ7mIlXjmtz32xAaHw+FwdMQIuAE+gajlEgJBYkOdawUBq+XKhpAwMXag0PYh+ciok/baUNsHwNc2sIDGlM1i0CPvLU9RgDZBasiGMQ2yGLQxItpEYDdGVHFC+oaJkDXgHHgGfoB7YF/DqaZIV60JxWv5RM81EZn/UiEPFAt5VHJQSgREJlNrq6R+swVn/oyJkNeS+i/h922lJYWYCPkuqZ8ZtKHGusG7eyX1u8Lv1TMHabCPKR7qsZ5rIoxXLY94D1kU8UL3OZqxEIAjloUcNzDYdrAbL78A85rnTpGOyAi4Zbl37oANPddEiKfWNnACPFE81DPgFNhRdLaKWiE+MCWePpK5OwfOsJ9R1wq5QCZgtUxrDKoEe5WQ94aG3roQ4v7Z+4YT0jeckL7xb4RIfqzK0hfbh96V9nziKzeID4SHRupzCPGRvMmO2tYO3aa9S4jT84D8Cm5IQj4SEVYuWx0ORw/4BVdkfPTcy1fHAAAAAElFTkSuQmCC"/>
                                <div class="ml-4 text-lg leading-7 font-semibold"><a  class="underline text-gray-900 dark:text-white"> Prénom : </a></div>
                            </div>

                            <div class="ml-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                              {{$last_name }}
                                </div>
                            </div>
                        </div>

                        <div class="p-6 border-t border-gray-200 dark:border-gray-700">
                            <div class="flex items-center">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAADzUlEQVRoge3YW4gcVRAG4G8STVzdxER9UYxhQdBEwRsImigEL0iiIioo+qTgDRQvi/sovuiqD74IgoiYQAwoiWgUBUWFxJcoKkLM6goK2ajgJcbLut6S9uHUOJNxeqe7Z5bd4PxwOH26qutyqruqTtNHH3300ccsYQEexdfIDrHxFUbDB4/MAYO6HQ8LrzKcnx+0OYtVIjK1uIDa7NnTFTKYN9tW9Ar/e0cuxPP4En9JGe9xzO+RXZVQ//KL4kEc0D57TOC+XhvYAf/aX8aRiyUnJnE3lmEhzsB4k6x7emzsdKjkyObgvTeHfk3Qd3drXQlUcuTz4D0lh17Dz8GztIQxNVwrbdQE/sCPeFeK7lHTPFvJkd3Bu3want+CZ3FBmcvxnmT8M7ga52INRvCFtIErc56v5Mj7wXtJDv2YoE8qlsFOkjLeOE7N4TkSr0ibuKQNPUNWNv3uinlFDv2CmD/C/g6yalIKX4hL8SkGcTvWY5PUzK7EjRjA8HQCy0Tk4+AdyaHfHPS9BWRd1SLrbI2+bwp/x/UTQX8Sn7SRUykiP8Wc976eFvOvBWTdEEZslL6nV3E87sIivBB8YzF/hqHpBJaJyGX4U6olx7XQDpcy1gHFiuKEFAGS8Rm2NtG3x701sX5AYyObUeljhy3B32rsurj/QUE5v2NnXG+MZ++P9aCUMDKcGPe2Sum4FZUduSL49zv4o59Srqp/pxGRp+PZZ6XMtKHJrqW4KPTd1EZOZUdIO5nhzFgf0STnsIIy3gz+ISmdN/dv43gurveEE5u0b3IzZEWVNmOB/1ba5lQ7iH0F5GyQercR3BHXa6VvZ30YuEuKyNt4rZPAohE5OhSOBf/3Dm5Ddmj0WcM4toO8eXhLisSdBfTnofCrtUzK4ZNNvGNSG9GMIanVqPNM4SmpeudhCV4P/m24Dauls/h1Ustyci8cuVJq3jJp597B9eL3SxvMl3qlNzTe+X0hJw81qTi+hG+kg9q3+BCP4YRuHTlHqhkZXsbpHQS2YkUYl0kN4Vklny+Kjo68GPe3dKmoLmdzl3Ly0NGRvU20XowfZtKRvP9aA9K5otcYkCp6L5GRjG/nyKGEjP5/rbmHoo6sk4rdLzGvnTGLutDVqbJfrn0Wmglnqugq3KLUfzgMS83icKx3dGt1j3QVdqTeYy2K9eJYFznOlkUVXYXP7PUD/60x3xLzzja83aIrXfU/F6ty6OdpdLOkgpbF/V6jrK7VGocvo4q3GUrwdjvK6HqI1JKPakQmb2wL4dtLKKg6iuraE07kHSv66KOPwD9M7hl6iNVmhQAAAABJRU5ErkJggg=="/>
                                <div class="ml-4 text-lg leading-7 font-semibold"><a  class="underline text-gray-900 dark:text-white">Téléphone & email : </a></div>
                            </div>

                            <div class="ml-12">
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                  {{$phone }}
                                  <br>
                                  {{$email }}
                                </div>
                            </div>
                        </div>

                        <div class="p-6 border-t border-gray-200 dark:border-gray-700 md:border-l">
                            <div class="flex items-center">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QA/wD/AP+gvaeTAAAE90lEQVRYhcWWe4hVVRTGf2vPvYcpvWNUhESQUCBR1Ny9T00+oqkoY4IKwjCCyB72gp6SvYtSSKEHUn/0wggrKsMKLISoi1bTxDl7ChozMKI/QgsS9Whdz4x79cc9V2567d6Rwg8OnLP32uv71nfW3mzhMOGcGwBeKj5vSdN05HDyyGQXDA4O9mZZ9gSwGOgphoOIvKqq96Rp+uf/JqCoehVwGrBPVZ8BEJH7CjE/AAsn40ZXAtpUvUVVb/Deb2wjbFJu9HQKcM4N5Hn+CXAlgIi8Alzpvf+pGbN169ZfZ86c+Vqe52VgNnA2sGD69Onfbtu27Zd/y39IBzpV/W+CmYQbbQW0+dcrRWQ7sACYCfwM1IwxT4cQBFgCXADMADar6tsichxwFx164x8C2lT9g4jcrKqPAvPaaM2KHFPbzH1sjFkWQni1pZBn+vr6Hq/VavVmkGmtOssyX1SDqq6oVCpWVc8syH8TkaFKpXIUcCbwAVApyNcaY86IouhoEbkC+AMYCiGcVeRYASAi92dZ5guHGw60q7rVLufcBuA8EZmfJMmaFrdKWZZ9DGilUrmsVqtNNOestVeLyDsisiFJkvObBXLAFu7r63tc4jj+SlVntbHwQByTpunOLuJwzk0DdnQR+rXpkpw8z03nqAaiKOq4vQucW2r5+ALYGUXRdcPDw9ubg865FLC9vb0O+LRLsXHxmqZp2nxn1qxZx+Z5/gYwDZgLLU1YqVQuADbneZ5Wq9X9i1T1I4AQwj1dVoWINGM/bI5Vq9U4z/MU2Fxw8Q8BtVptIk3Txap6rzFmnXPuNoBSqfQCjf855Jy7sxO5tfZuVb0U2BFF0YsAzrnbjDHrVPXeNE0XtzbsQf/Ve79WVecAi5xzq+v1el1EbgYUeN5a+xDtDzATx/HDIvIsoCJy0549e/Y651YDi1R1jvd+7UGL2lXhvd8SRdFsVd1bLpe/CSGMAXcUiZc559b39/fPaMb39/fPsNauV9WlhdDbQwibyuXyN6q6N4qi2d77Le24xDmnAGmatj2WrbULRWS5iNwJ7FTV14ETgD0i8iSAqj4GTAF+F5HrgWmqulJVl3jvV7XL2+TtuLW896tCCBer6pMhhCFjTAy8BUxR1eWqurwgf8sYE4cQhorYiw9F3oqu9vbo6Oh3pVIpNsacGEJYAzxI43jeVDzzgAdDCGuMMSeWSqV4dHT0u25ylzqHNDAyMrILuMpau0hERkTkhiRJTgew1l4oIsPAyiRJVtDog67QtYACIiJTAVT1Zedc0+KFNNwcn2S+7gUMDAz07du3b5WqngQMjI+P18vl8mqA8fFxVy6Xe4H34jie09PTs7Bw7L8RYK09bWJi4n0R2Viv168ZGxvLi6lLWuMGBwfnZFm2dGJiIqlWq/O76YOOTRjH8XUiUhORp5IkuaWF/CAUp+kDqrrEGLPeWruoU/5DOlDcE5ar6jwRuShJku87JWvCe782juNNwHvW2rkicuuh7oRtHahWqyfv2rVrA3B8vV6PJ0PeRJIkP0ZRNECjMb+01p7alYA4ji83xoyIyJtpml47Nja2e7LkTQwPD//lvb9RRJ4TkS+dc/MPjNl/FFcqlfLu3bsfUdXrVXWB9/7rwyVuB+ecBd4F1tG4/uXQ4kCWZZ+rahxFkf2vyQHSNPVRFJ0DnAJ81hzf78CRgqFxFTtS2Pg3Amtn6ORpjLQAAAAASUVORK5CYII="/>
                            <div class="ml-4 text-lg leading-7 font-semibold"><a  class="underline text-gray-900 dark:text-white">Message : </a></div>
                            </div>

                            <div class="ml-12">
                         
                                <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                                {{$messages }}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="flex justify-center mt-4 sm:items-center sm:justify-between">
                    <div class="text-center text-sm text-gray-500 sm:text-left">
                              

                    </div>

                  
                </div>
            </div>
        </div>
    </body>
</html>
