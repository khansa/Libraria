<div class="p-6 sm:px-20 bg-white border-b border-gray-200">
    <div>
        <x-jet-application-logo class="block h-12 w-auto" />
    </div>

    <div class="mt-8 text-2xl">
Bienvenue dans notre site    </div>

    <div class="mt-6 text-gray-500">
     Merci pour votre inscription à notre site, veuillez explorer les fonctionnalités à votre disposion.
    </div>
</div>

<div class="bg-gray-200 bg-opacity-25 grid grid-cols-1 md:grid-cols-2">
    <div class="p-6">
        <div class="flex items-center">
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABmJLR0QA/wD/AP+gvaeTAAAE40lEQVRIicWXfUwTdxjHP1fKoVSwMCOmxoKihilmSZ3rYhVhlAzdljHYAN/2EpORvRhiFodu8yUjA8PcwBlMNDKYONgYupit26LJxqQYrUGMTBABqQyZFF8wQgPttbc/cMVKgS2N7pv8krvfPb/n83su33vuDiATpdgPyI9kBKmGgAwBpdhPtEHFopfA7YRJUxEC+mFKGEhOcDkgWI3fsveBQkTRYML1x3GHAMisL4PeDmirgY01kC3AR79Bcw001cC2Gv/BufGwIB5BsxB5bzrD4KQc0D59r7pwkHpAHQaSY3hOFe4/eOAWBIgIHQ3I3398D/w/SAGwZ88eurq6POPmzZtIkvRQRkVFBQDKtWvX/hwdHb3SbDZTXLwPAEEAZWDguDueqdFQWlrKZ4WFHDP9MmGFggA7tuZ4zpVWq3Wa3W7HYrHQYbWS+srqCZN0dXVSXl5OaWkpR4/9yBmbRPTiZahF+NMOtsHRa5SnKzGbzcTExAyf19XVLUlPTwcgMmo223J3AWDruc6UKSEEq1S0Xr6Erec6AFptFJ2dVo5WVY5kXWRkTc5OnlBD5VU4cg2ClfCkGmxDcOkuKDvOeG1E4asip9PJqkQDm7PfAuCL3btISU4gJTmBysNlPu9CbhO8fGoYCvDcDKg2wKGnfN81n+CqikP81X2NI1UVtLdeZu+BMnoHZHoHZN7/cKfvTAAd9SizI6GmhO+6YPqmEpbGRkJH/ahQpa/1RbvzCQkJ5U7fbfYWFjA7ei6/nhg20Op1rzMrMsonNDDfyBKtwOmDb+Ju/h2h7mv0MaFY8o1IwVOBuPErtl5pZ8UzRnRL9FivtDNvfgyG5fEYlsf7hlrPE5hvJFOvojZvAQfenoNgPsz6FdM4mbeQTL0KyXZ14or3l1YQ9lg4DocDx5CDqWo1hrh4YMRcXqo/RmbiTMo2RqEQYENSBABZ+9pZERtK2cYoLnQMTAzOemONr2kANn+ww7MJAFGQeEE/3QP9RxuSIhAE0ISLKAQIC/HuCz7BvQPjd9G62pGXxuSgIH6otRGQYht3DUBSetD44BeT431NA6PNdfDLMpqbmyeECoKAXq/HZDKNDTYsHxv8oLlaWlqorx/9uPiSVqv1HPsGx40NftBc23M/4ezlTpQRc8aFSu31DA4OjrRMX0EpyQljJnjQXG63jGRYj5R2r7E01cB5EwSpYNk6iJgLwKQ875x+mauvr89rXvjpc4TKHOISn+VG9w2aTZ/i2nIC5i8dlcMvcw0ODuKUnMMX7HcQvt1KUfEB6s+ewZhopOliLEe+2czQ9rp/B/4v5vJ8v/S04XY6GBoa4quS/QAU7jtIVXWVV7hGoyE5OfmW3+byaMY8FIFBiEEir23IYt78GCynTxGgjUW6L0ytVqPT6Qb8NpdHk0ORM/LZ9E4WyxKSOHvuHC3NTbi2HPeEuN1uGhsbycvLm+V357pf8qpNyLN1nGwwwRwVvFrhcbWMjN1uR6vVUlRUVOIFvth4gbTnjeNCAe703UYQhhuzQhBQmssJaDWPDmyt9Ry6rBeAxbS1tVFQULBSCeByuUhNTcXhcOB2uycEA6zJzADgvex3qa2tnSAaFHGPk5qaisViobu7WyOIotiv0+lUaWlpnioelmRZprq6moaGhn6ADFF8dD9toijeBdL/Bl7eTnGg579UAAAAAElFTkSuQmCC"/>
            <div class="ml-4 text-lg text-gray-600 leading-7 font-semibold"><a href="{{route('blog')}}">Participation au Blog</a></div>
        </div>

        <div class="ml-12">
            <div class="mt-2 text-sm text-gray-500">
               Grace à votre compte vous pouvez intéragir avec les autres membres en lisant & écrivant des commentaires relatifs au blog.
            </div>

            <a href="{{route('blog')}}">
                <div class="mt-3 flex items-center text-sm font-semibold text-indigo-700">
                        <div>Explorer les blogs</div>

                        <div class="ml-1 text-indigo-500">
                            <svg viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                        </div>
                </div>
            </a>
        </div>
    </div>

    <div class="p-6 border-t border-gray-200 md:border-t-0 md:border-l">
        <div class="flex items-center">
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABmJLR0QA/wD/AP+gvaeTAAABd0lEQVRIie1WPUsDQRSctwloIQqSpLCy8TcIFnrrD0hpYRUxORQEbVKbtGKnjfnA/Akri7uAiD/Aws6UehdFLCwk7rNSckHJvpN4BJ1y7r0ZuNm5W+CvgaQLmaLnKAWnnzMGfrehfYlOWmqsFBxm2o9yDAAiYyU1HnuIM86WvAKgClHWtMK6bkl0xBkTYZ6ZVwY4Ub6xjJnRAagd5bgj1UkM4oxzrlcZrBMRV4Oarkh0EqvTv/GvYejhyhX9MhMOZKJcDur68EfGAJBz/RNmuJa+rbDubAwbsnrVAaZ2AHgWoxczky9bNprWPZ7evJydUK9XABa+GblN9dTi3elyaKNnfbiem0uPhjgP4Omrx8pw3tZUZAwADzV9Q4w1AL0++o3JrN839bVES1ynoOGcM/H2J8G0262tnkl1YiPresfZUvso7r74t/iBcA57gIm7nhwidcoUPYeIbPoqBjPr/itwYt/qSMbpNDrGcHUURqkUxud6NBK8A34mcp4fzCnTAAAAAElFTkSuQmCC"/>
            <div class="ml-4 text-lg text-gray-600 leading-7 font-semibold"><a href="{{route('books-media-list-view')}}">Téléchargement des Livres & Videos</a></div>
        </div>

        <div class="ml-12">
            <div class="mt-2 text-sm text-gray-500">
               En tant que membre vous pouvez visualiser & télécharger les livres/videos de facon illimitée.
            </div>

            <a href="{{route('books-media-list-view')}}">
                <div class="mt-3 flex items-center text-sm font-semibold text-indigo-700">
                        <div>Explorer tous les livres</div>

                        <div class="ml-1 text-indigo-500">
                            <svg viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                        </div>
                </div>
            </a>


            <a href="{{route('video')}}">
                <div class="mt-3 flex items-center text-sm font-semibold text-indigo-700">
                        <div>Explorer tous les videos</div>

                        <div class="ml-1 text-indigo-500">
                            <svg viewBox="0 0 20 20" fill="currentColor" class="w-4 h-4"><path fill-rule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                        </div>
                </div>
            </a>
        </div>
    </div>

    <div class="p-6 border-t border-gray-200">
        <div class="flex items-center">
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABmJLR0QA/wD/AP+gvaeTAAAD8ElEQVRIib2XWUxcVRjHf3dYCyVgkY4shbIMtKCVNezWCnaAKhqKoPShRkODfeFFfaDBEI12MRbrltANBFIatoQoKbYJ2NCIxtRES0M7rTVa1gBDUsMM3Ll3xgdkypQ7wzAS/8l5OOd85/ude+53/jcXlBUVHhE6JQiCBXCpqVQqS3hE6CQQqQRQKQ3GxEadrXijauu4QWRyQXKpjc0v8trrh9TRmsgzToPdVKro1PQMBEGwcyBrSxAE0jKzcHdz0zgN/j/kFLi/t4fkbUE84e3usCWHB9F/6VubtWaLRTGnuzPgdw9X0lFbTFZCmMO4a8P3qThcyc9/TCxBAZO4sOgyeHx6jsz4JejfBpGfbo2BJIMsAZCmUePv60X29gDGpmaQAMkMBhnMZrPZZfBK6e7PcqJ1EMu/+QQB6l5JJitOvXSRgAdL+0H5kF0Ep4R4c/mIdr3LVml9VS2ZQBT/M9QFsLQh0PWDLYp1sjFgcbQuI9BfFbRRgJAg7zBxtC7DEbjiZK1Wh8FzyMudTRsF3uTp5ovBc+jTukIdUK4Ejp7Vi4q+GqYO5Opvf4IdF1rWwM1xwkLUinPT0wsaIHa5r3id3q56jpjEJGv/REMTB996k7/GphyCI7YFU/fleWs/PjGJd6qeVYy1AbdfusFTccEU70vlQUCAdTw1v4C+22MOoUry8w8gMUez5HKCbDNnU1zRkY9T3zRISuHHdLVdQJZlLBYQXShmWZYZ/uU6FncVMjokadw+OC0pnB+/q+bU+9m0nf6QnF07aP26EclkchoomUx0NzfyYpKG0+8dxMtjEknWr4pTvMfP745gsPsFWurTGeiqp2hXFC1fnWLBaLQLNIkiPReaKU6Jo7fpI84eS+dK2z678Q4NJDstlN7mQr5pzEd3rQltfATnTh5nwWCwxoiLi7SfP4M2YTtXWo9y8fMcfugpIS833FFq5z4SiQlb6WzYy683p/ngszYKnvyEgtJysFjo62onNyOEvpYCnk5w3necAuvuzTExNc/uzDA6G7Tcuqunu/cGADXdLxEXvQWAq0OjBKt9iY16bM2ca3r1yB09e/a3Mzv38P3uiNlCTXU6NdXpVijA7JyRPfvbGbmzupgelcMnvv27nr3lnRw7kktJkaKp2aikSIMkmckv6+DyxVIS4gLXD9bdmyOvtIPjtc9woGTnmtBllRXHYZLMaF/tZKCrDI2dY7cLnpk18sXRPF4uiHEauqwDJTvx9fFgRm90Cmw0Gh8aRVZayLqBK/Xoho2LZgBroawsru/buq9LwyO21rYRGrk7T0fvhAT0Kwb4+Hgc8tvsOYOLP2r2mt9mjxkfH4/Klax/ADW4mqn84xtfAAAAAElFTkSuQmCC"/>
            <div class="ml-4 text-lg text-gray-600 leading-7 font-semibold"><a >Newsletters</a></div>
        </div>

        <div class="ml-12">
            <div class="mt-2 text-sm text-gray-500">
              Grace à votre compte, vous serez notifier de tous les news sur notre site.
            </div>
        </div>
    </div>

    <div class="p-6 border-t border-gray-200 md:border-l">
        <div class="flex items-center">
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABmJLR0QA/wD/AP+gvaeTAAAByElEQVRIie3WT29NQRgG8B83FmXZINrrlitW9EtYlrRXWJD4CuqzWDbYCIlYUMI36cKfaAjSda+NVpXFmTp/cu7MOaciFn2SycnMnOd95p1533mHA/wjHGrx7wBLuIwz6IfxL/iIV3iBz39rcbNYwQ5+JdpPPA0L2xdG+NZAsNrGWOwqekfmQVvRovfLbUVH+xQtijf2vC++ve+wgGM4iUcJ8TFmmgg/iBjZxvkaziVsRXj3UqID8ehdjXBXI7wdefqBwxXyEnoR4+8jc28ic71ge6LwQoQM05G5EwluyXZVeJggH+k4B+diwqcS5Bn1R9GT3XAp7kSMpXPzWg3vegPeOCb8toGBD8pnPR3GUrxS8FW3ej22qoAh5gv9eenYEBY3Ufh1AwOfsFbor2lWCqO2T6u/QLbwWHaHT9XwpnA1/LNdw/+hcoHU4X6F9BUX0g79wUVsVGysNCHOKkd3l7o6KvA31aRp9YzJPLyF3dCf6yA8CN/dYGujDXlZXpOfFYzFMIfn8lp8u41gEYvybf+OJ7ghS5+joQ1xU/bW2iuNm7jSVXQPx3FXFplNXhwPpa/eVs/bvvx5e1b5ebsuy9OXoX+A/we/AVA55aUaIMIJAAAAAElFTkSuQmCC"/>
            <div class="ml-4 text-lg text-gray-600 leading-7 font-semibold">Profil</div>
        </div>

        <div class="ml-12">
            <div class="mt-2 text-sm text-gray-500">
              Vous pouvez modifier votre profil à volantée et à tous moment.
            </div>
        </div>
    </div>
</div>
