            @extends('layouts.master')
            @include('CRUD.style')

@section('content')
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        Ajout /Edition/Suppression Book/Media        </h2>


        <br>
        <a href="{{route('show_index_admin')}}" class="colorlink">  <input type="button" value="Retour au menu" > </a>
    </x-slot>

    

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">

            </div>
            
  
            <a href="{{route('crudadd','Books')}}" class="colorlinkadd">  <input type="button" value="Ajouter Book/Media" > </a>


    <table class="table table-bordered" id="users-table">
        <thead>
            <tr>
            <th> ID </th>
 <th>Title</th>
 <th>Author</th>
 <th>Rating</th>
 <th>Edition</th>
 <th>Description</th>
 <th>Length</th>
 <th>Format</th>
 <th>Language</th>
<th>Publisher</th>
<th></th>
                <th>Actions</th>
            </tr>
        </thead>
    </table>



    </div>
    </div>
</x-app-layout>
@stop

@push('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        ajax:{
            "url" :  '{!! route("data") !!}',

            "type": "POST",
            "data": {
                
            'data' :     "Books"
            }

        },   
        
        "order": [[ 0, "desc" ]],
        
        
             columns: [



            { data:'id' ,name: 'id' },
   { data: 'title' ,name: 'title' },
 { data: 'author' ,name: 'author' },
 { data: 'rating' ,name: 'rating' },
 { data: 'edition' ,name: 'edition' },
 { data: 'description' ,name: 'description' },
 { data: 'length' ,name: 'length' },
 { data: 'format' ,name: 'format' },
 { data: 'language' ,name: 'language' },
 { data: 'publisher' ,name: 'publisher' },
            { data: 'editer', name: 'editer' ,'searchable' : false, 'orderable' :  false},
            { data: 'supprimer', name: 'supprimer' ,'searchable' : false, 'orderable' :  false}

        ],


scrollY:        false,
scrollX:        false,
scrollCollapse: true,
paging:         false
   });
});
</script>
@endpush

       
