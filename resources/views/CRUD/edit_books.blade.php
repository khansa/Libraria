@extends('layouts.master')
@include('CRUD.style')


@section('content')
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        Editer un Book   numéro : {{$id}}     </h2>

        <br> 
  <a href="{{route('show_index_admin')}}" class="colorlink">  <input type="button" value="Retour au menu" > </a>
    </x-slot>

    

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">

            </div>

            <form action="{{route('crudedit',['model' => 'Books','id' => $data->id ])}}" method ="POST" enctype="multipart/form-data">
  <label for="ltitle">Titre:</label><br>
  <input type="text" id="title" name="title" size="70" value="{{$data->title}}" required><br><br>



  <label for="lauthor">Author :</label><br>
  <input type="text" id="author" name="author"  size="70" value="{{$data->author}}" required><br><br>


  <label for="limg">Image : (Ancienne Valeur : {{$data->img}})</label><br>
   <input type="file" id="img" name="img" accept="image/*" value="{{$data->img}}" required><br><br>



   <label for="ledition">Edition :</label><br>
  <input type="text" id="edition" name="edition"  size="70" value="{{$data->edition}}" required><br><br>




  <label for="ldescription">Description :</label><br>
  <textarea  type="text" id="description" name="description"  rows="5" cols="90" required>{{$data->description}}</textarea>

  <br><br>


  <label for="llength">length :</label><br>
  <input type="number" id="length" name="length" min="0"  size="10" value="{{$data->length}}" required><br><br>
  <br><br>

  <label for="llanguage">Langue :</label><br>
  <input type="text" id="language" name="language" min="0"  size="10" value="{{$data->language}}" required><br><br>
  <br><br>

  <label for="lcategory">Category :</label><br>

 
  <select name="category_id" id="category_id">

  @foreach (App\Models\Book_categories::all() as $category )

<option value="{{$category->id}}">{{$category->category}}</option>
@endforeach

</select><br><br>

<label for="lcategory">Rating :</label><br>

<select name="rating" id="rating">


<option value="1">1</option>
<option value="2">2</option>

<option value="3">3</option>

<option value="4">4</option>

<option value="5">5</option>


</select><br><br>


<label for="lformat">Format :</label><br>

  <select name="format" id="format-select">


  @foreach (App\Models\Format_books::all() as $Format_book )

<option value="{{$Format_book->format}}">{{$Format_book->format}}</option>
@endforeach



</select><br>

<br>



<label for="lpublisher">Publisher :</label><br>
  <input type="text" id="publisher" name="publisher" value="{{$data->publisher}}" size="70" required><br><br>




<label for="lurl">URL : (Ancienne Valeur : {{$data->url}})</label><br>
<input type="file" id="url" name="url"
  accept=".pdf"><br><br>
 




  <br><br>
     <input type="submit" value="Envoyer">


</form> 


            
    </div>
    </div>
</x-app-layout>