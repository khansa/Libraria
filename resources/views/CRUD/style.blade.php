<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
function submitResult(e) {
    e.preventDefault();
    var urlToRedirect = e.currentTarget.getAttribute('href');
    console.log(urlToRedirect);
    Swal.fire({
        title: 'Message de suppression',
        text: "Etes vous sur de vouloir supprimer ?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmer'
    }).then((result) => {
        if (result.isConfirmed) {

            Swal.fire(
                'Supprimé !',
                'La ligne selectionnée est supprimée ',
                'success'
            )
            window.open(urlToRedirect, "_self");
        }
    })
}
</script>

<style>
.colorlink:link, .colorlink:visited {
  background-color: #F26D35;
  color: white;
  padding: 15px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

.colorlink:hover, a:active {
  background-color: #F26D35;
}



.colorlinkadd:link, .colorlinkadd:visited {
  background-color:blue;
  color: white;
  padding: 6px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}


.colorlinkedit:link, .colorlinkedit:visited {
  background-color:orange;
  color: white;
  padding: 6px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}

.colorlinkdelete:link, .colorlinkdelete:visited {
  background-color:red;
  color: white;
  padding: 6px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
}


input[type=submit] {
  background-color: #4CAF50;
  border: none;
  color: white;
  padding: 16px 32px;
  text-decoration: none;
  margin: 4px 2px;
  cursor: pointer;
}

</style>