@extends('layouts.master')
@include('CRUD.style')


@section('content')
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        Ajout d'un nouveau even news     </h2>

        <br> 
  <a href="{{route('show_index_admin')}}" class="colorlink">  <input type="button" value="Retour au menu" > </a>
    </x-slot>

    

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">

            </div>

            <form action="{{route('crudadd','News')}}" method ="POST" enctype="multipart/form-data">
  <label for="ltitle">Titre:</label><br>
  <input type="text" id="title" name="title" size="70" required><br><br>




  <label for="limg">Image :</label><br>
   <input type="file" id="img" name="img" accept="image/*" required><br><br>



  <label for="lauthor">Author :</label><br>
  <input type="text" id="author" name="author"  size="70" required><br><br>





  <label for="lcontent">Content :</label><br>
  <textarea  type="text" id="content" name="content"  rows="5" cols="90" required></textarea>

  <br><br>


  <label for="lcontact_info">Contact Info :</label><br>
  <textarea  type="text" id="contact_info" name="contact_info"  rows="5" cols="90" required></textarea>

  <br><br>


  <label for="llocation">location :</label><br>
  <input type="text" id="location" name="location"  size="70" required><br><br>


  <label for="llocation">Start Date :</label><br>

  <input type="time" id="start_time" name="start_time"
        required><br><br>

        <label for="llocation">End Date :</label><br><br>

       <input type="time" id="end_time" name="end_time"
        required>

  
 


  <br><br>
     <input type="submit" value="Envoyer">

</form>
            
    </div>
    </div>
</x-app-layout>