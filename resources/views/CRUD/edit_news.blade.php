@extends('layouts.master')
@include('CRUD.style')


@section('content')
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        Editer un Event News  numéro : {{$id}}      </h2>

        <br> 
  <a href="{{route('show_index_admin')}}" class="colorlink">  <input type="button" value="Retour au menu" > </a>
    </x-slot>

    

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">

            </div>

            <form action="{{route('crudedit',['model' => 'News','id' => $data->id ])}}" method ="POST" enctype="multipart/form-data">
  <label for="ltitle">Titre:</label><br>
  <input type="text" id="title" name="title" size="70" value="{{$data->title}}" required><br><br>




  <label for="limg">Image : (Valeur Actuelle : {{$data->img}})</label><br>
   <input type="file" id="img" name="img" accept="image/*"  required><br><br>



  <label for="lauthor">Author :</label><br>
  <input type="text" id="author" name="author"  size="70" value="{{$data->author}}" required><br><br>





  <label for="lcontent">Content :</label><br>
  <textarea  type="text" id="content" name="content"  rows="5" cols="90" required>{{$data->content}}</textarea>

  <br><br>


  <label for="lcontact_info">Contact Info :</label><br>
  <textarea  type="text" id="contact_info" name="contact_info"  rows="5" cols="90" required>{{$data->contact_info}} </textarea>

  <br><br>


  <label for="llocation">location :</label><br>
  <input type="text" id="location" name="location"  value="{{$data->location}}" size="70" required><br><br>


  <label for="llocation">Start Date :</label><br>

  <input type="time" id="start_time" name="start_time" value="{{$data->start_time}}"
        required><br><br>

        <label for="llocation">End Date :</label><br><br>

       <input type="time" id="end_time" name="end_time" value="{{$data->end_time}}"
        required>

  
 


  <br><br>
     <input type="submit" value="Envoyer">

</form>


            
    </div>
    </div>
</x-app-layout>