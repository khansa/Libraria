@extends('layouts.master')
@include('CRUD.style')


@section('content')
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        Ajout /Edition/Suppression Blog        </h2>

        <br> 
  <a href="{{route('show_index_admin')}}" class="colorlink">  <input type="button" value="Retour au menu" > </a>
    </x-slot>

    

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">

            </div>
            
     

            <a href="{{route('crudadd','Blog')}}" class="colorlinkadd">  <input type="button" value="Ajouter Blog" > </a>

    <table class="table table-bordered" id="users-table">
        <thead>
            <tr>
            <th>	ID</th>
 <th>	Title</th>
 <th>	Theme</th>
 <th>	Views</th>
 <th>	Content</th>
 <th>	Author</th>
 <th>	Author_description</th>

 
 <th></th>
                <th>Actions</th>

            </tr>
        </thead>
    </table>



    </div>
    </div>
</x-app-layout>
@stop

@push('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax:{
            "url" :  '{!! route("data") !!}',

            "type": "POST",
            "data": {
                
            'data' :     "Blog"
            }

        },        columns: [



            { data: 'id', name: 'id' },
{ data: 'title', name: 'title' },
{ data: 'theme', name: 'theme' },
{ data: 'views', name: 'views' },
{ data: 'content', name: 'content' },
{ data: 'author', name: 'author' },
{ data: 'author_description', name: 'author_description' },
            { data: 'editer', name: 'editer' ,'searchable' : false, 'orderable' :  false},
            { data: 'supprimer', name: 'supprimer' ,'searchable' : false, 'orderable' :  false}






        ],


scrollY:        false,
scrollX:        false,
scrollCollapse: true,
paging:         false,
columnDefs: [
    { width: 200, targets: 0 }
]
    });
});
</script>
@endpush

       
