@extends('layouts.master')
@include('CRUD.style')


</style>
@section('content')
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        Ajout /Edition/Suppression Utilisateurs        </h2>

<br>
<a href="{{route('show_index_admin')}}" class="colorlink">  <input type="button" value="Retour au menu" > </a>
    </x-slot>

    

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">

            </div>
            
            <a href="{{route('crudadd','User')}}" class="colorlinkadd">  <input type="button" value="Ajouter User" > </a>



    <table class="table table-bordered" id="users-table" >
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>

                <th></th>
                <th>Actions</th>
               
            </tr>
        </thead>
    </table>



    </div>
    </div>
</x-app-layout>
@stop

@push('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax:{
            "url" :  '{!! route("data") !!}',

            "type": "POST",
            "data": {
                
            'data' :     "User"
            }

        },        columns: [
            { data: 'id', name: 'id' ,width : "10px" },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'role', name: 'role' },
            { data: 'editer', name: 'editer' ,'searchable' : false, 'orderable' :  false},
            { data: 'supprimer', name: 'supprimer' ,'searchable' : false, 'orderable' :  false}

  
        ],


scrollY:        false,
scrollX:        false,
scrollCollapse: true,
paging:         false,
columnDefs: [
    { width: 200, targets: 0 }
]
    });
});
</script>
@endpush

       
