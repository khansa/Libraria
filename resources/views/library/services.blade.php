<!DOCTYPE html>
<html lang="fr">
    

@include('library.header_inc')

    
    <body>
        
       @include('library.topbar')
                
        <!-- Start: Page Banner -->
        <section class="page-banner services-banner">
            <div class="container">
                <div class="banner-header">
                    <h2>Nos Services</h2>
                    <span class="underline center"></span>
                    <p class="lead">Profiter de nos services en ligne</p>
                </div>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="{{route('index')}}">Accueil</a></li>
                        <li>Services</li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- End: Page Banner -->
        <!-- Start: Services Section -->
        <div id="content" class="site-content">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="services-main">
                        <div class="services-pg">                            
                            <section class="services-offering">
                                <div class="container">
                                    <div class="center-content">
                                        <h2 class="section-title">SERVICES fournis</h2>
                                        <span class="underline center"></span>
                                        <p class="lead"></p>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="contact-location">
                                    
                                        <div class="flipcard">
                                            <div class="front">
                                                <div class="top-info">
                                                    <h3><i class="fa fa-microphone" aria-hidden="true"></i><span>salles de réunions en ligne</span></h3>
                                                </div>
                                                <div class="bottom-info">
                                                    <span class="top-arrow"></span>
                                                    <p>Simplifiez vos visio en reservant une salle de conférence ou salles de réunions de Libraria</p>
                                                    <a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                            <div class="back">
                                                <div class="bottom-info orange-bg">
                                                    <span class="bottom-arrow"></span>
                                                    <p>Simplifiez vos visio en reservant une salle de conférence ou salles de réunions de Libraria</p>
                                                    <a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                                </div>
                                                <div class="top-info dark-bg">
                                                    <h3><i class="fa fa-microphone" aria-hidden="true"></i><span>salles de réunions en ligne</span></h3>
                                                </div>                                                
                                            </div>
                                        </div>
                                        
                                        <div class="flipcard">
                                            <div class="front">
                                                <div class="top-info">
                                                    <h3><i class="fa fa-book" aria-hidden="true"></i><span>Travail collaboratif</span></h3>
                                                </div>
                                                <div class="bottom-info">
                                                    <span class="top-arrow"></span>
                                                    <p>Avec la multiplication des outils du web, chaque utilisateur s'improviser veilleur, chercheur ou diffuseur d'informations auprès de son réseau.</p>
                                                    <a href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                            <div class="back">
                                                <div class="bottom-info">
                                                    <span class="bottom-arrow"></span>
                                                    <p>Avec la multiplication des outils du web, chaque membre d'une organisation peut s'improviser veilleur, chercheur ou diffuseur d'informations auprès de son réseau.</p>
                                                    <a href="#"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                                </div>
                                                <div class="top-info">
                                                    <h3><i class="fa fa-book" aria-hidden="true"></i><span>Travail collaboratif</span></h3>
                                                </div>                                                
                                            </div>
                                        </div>
                                        <div class="flipcard">
                                            <div class="front">
                                                <div class="top-info">
                                                    <h3><i class="fa fa-briefcase" aria-hidden="true"></i><span>Formation</span></h3>
                                                </div>
                                                <div class="bottom-info">
                                                    <span class="top-arrow"></span>
                                                    <p>Libraria offre des formations gratites pour développez vos compétences en ligne</p>
                                                    <a href="#"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                            <div class="back">
                                                <div class="bottom-info">
                                                    <span class="bottom-arrow"></span>
                                                    <p>Libraria offre des formations gratites pour développez vos compétences en ligne</p>
                                                    <a href="#">View Selection <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                                </div>
                                                <div class="top-info">
                                                    <h3><i class="fa fa-briefcase" aria-hidden="true"></i><span>Formation</span></h3>
                                                </div>                                                
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </section>
                            <section class="who-we-are">
                                <div class="company-info">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-8 border">
                                                <div class="row">
                                                    <div class="col-sm-11">
                                                        <div class="company-detail">
                                                            <h3 class="section-title">Que Somme Nous?</h3>
                                                            <span class="underline left"></span>
                                                            <p> Notre site traite de la culture générale au sens large: 
                                                                des livres divers , Arts, nouveautés et événements,… et bien d’autres choses. </p>
                                                            <p>Notre objectif?</p>
                                                            <p>Distiller chaque jour un peu de savoir pour vous permettre de briller en société et passer aux yeux de votre entourage pour un être exceptionnel
                                                                Au travers de vos pérégrinations sur le site, vous pourriez connaître des nouveaux livres dans le thème que vous choisissez, redarder des films que vous voulez, vous pourriez aussi découvrir des nouveautés, des évènements et plein d'autres rupriques... Bref, on espère que vous ne vous ennuierez pas avec nous!
                                                                Amis intellectuels, amis lecteurs, derrière l’apparente légèreté de ces lignes écrites se cachent des informations véridiques et
                                                                vérifiées, pour vous offrir du contenu de qualité</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="company-image"></div>
                                </div>
                            </section>
                            <div class="fun-stuff">
                                <div class="container">
                                    <div class="facts-counter">
                                        <ul>
                                        <a href="{{route('books-media-list-view')}}">

                                            <li class="col-sm-3">
                                                <div class="fact-item icon-ebooks">
                                                    <div class="fact-icon">
                                                        <img src="images/icon-ebooks.png" alt="" />
                                                    </div>
                                                    <span>Books<strong class="fact-counter">{{count_numberofbook('pdf')}}</strong></span>
                                                </div>
                                            </li>

                                            </a>

                                            <li class="col-sm-3">
                                                <div class="fact-item icon-eaudio">
                                                    <div class="fact-icon">
                                                        <img src="images/icon-eaudio.png" alt="" />
                                                    </div>
                                                    <span>eAudio<strong class="fact-counter">{{count_numberofbook('eAudio')}}</strong></span>
                                                </div>
                                            </li>
                                            <li class="col-sm-3">
                                                <div class="fact-item icon-magazine">
                                                    <div class="fact-icon">
                                                        <img src="images/icon-magazine.png" alt="" />
                                                    </div>
                                                    <span>Magazine<strong class="fact-counter">{{count_numberofbook('Magazine')}}</strong></span>
                                                </div>
                                            </li>
                                            <a href="{{route('video')}}">

                                            <li class="col-sm-3">
                                                <div class="fact-item icon-videos">
                                                    <div class="fact-icon">
                                                        <img src="images/icon-videos.png" alt="" />
                                                    </div>
                                                    <span>Videos<strong class="fact-counter">9</strong></span>
                                                </div>
                                            </li>
                                            </a>

                                        </ul>
                                    </div>
                                </div>
                            </div>  
               
                            <!-- Start: Category Filter -->
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <!-- End: Services Section -->
        
    
        
       @include('library.footerbar')
        @include('library.footer_inc')

    </body>


</html>