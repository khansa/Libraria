<!DOCTYPE html>
<html lang="fr">
    
@include('library.header_inc')


    
    <body>
        
    @include('library.topbar')
    @if(!empty($successMsg))
  <div class="alert alert-success"> {{ $successMsg }}</div>
@endif


@if(!empty($errorMsg))
  <div class="alert alert-danger"> {{ $errorMsg }}</div>
@endif
        <!-- Start: Slider Section -->
        <div data-ride="carousel" class="carousel slide" id="home-v1-header-carousel">
            
            <!-- Carousel slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <figure>
                        <img alt="Home Slide" src="images/header-slider/home-v1/header-slide.jpg" />
                    </figure>
                    <div class="container">
                        <div class="carousel-caption">
                            <!-- <h3>Online Learning Anytime, Anywhere!</h3>-->


                            <h3>Des savoirs en partage à tout moment, n'importe où!</h3>

                            <!-- <h2>Discover Your Roots</h2>-->
                            <h2>Culture Pour Tous</h2>

                            <p>Une librairie virtuelle contenant toutes les ressources culturelles afin de
                               faciliter l'accès à l’information et à la documentation.</p>


                            <div class="slide-buttons hidden-sm hidden-xs">    
                                <a href="{{route('services')}}" class="btn btn-primary">voir plus</a>
                            </div>
                        </div>
                    </div>
                </div>
         
         
            </div>
            
    
        </div>
        <!-- End: Slider Section -->
        
        <!-- Start: Search Section -->
        <section class="search-filters">
            <div class="container">
                <div class="filter-box">
<!--                     <h3>Que cherchez vous?</h3>
 -->
                    <h3> Que cherchez-vous à la bibliothèque ?</h3>

                   

                    <form action="{{ route('books-media-list-view') }}" method="get">
                        <div class="col-md-4 col-sm-6">
                            <div class="form-group">
                                <label class="sr-only" for="keywords">Recherche</label>
                                <input class="form-control" placeholder="une recherche mot-clé?" id="keywords" name="keywords" type="text">
                            </div>
                        </div>
                     
                        <div class="col-md-3 col-sm-6">
                            <div class="form-group">
                                <select name="category" id="category" class="form-control">
                                    <option value ="all">Les Categories</option>
                           @foreach ($categories as $category)
                           <option value ="{{$category->category}}">{{$category->category}}</option>

                           @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <div class="form-group">
                                <input class="form-control" type="submit" value="recherche">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <!-- End: Search Section -->
        
        <!-- Start: Welcome Section -->
        <section class="welcome-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="welcome-wrap">
                            <div class="welcome-text">
                                <h2 class="section-title">Bienvenue a libraria</h2>
                                <span class="underline left"></span>
                                <p class="lead">La bibliothèque en ligne est une collection de documents numériques en ligne gratuits </p>
                                <p>Libraria est une bibliotheque dont l'objectif est de distiller chaque jour un peu de savoir pour vous permettre de briller en société et passer aux yeux de votre entourage pour un être exceptionnel
Au travers de vos pérégrinations sur le site, vous pourriez connaître des nouveaux livres dans le thème que vous choisissez, redarder des films que vous voulez, vous pourriez aussi découvrir des nouveautés, des évènements et plein d'autres rupriques... Bref, on espère que vous ne vous ennuierez pas avec nous!
Amis intellectuels, amis lecteurs, derrière l’apparente légèreté de ces lignes écrites se cachent des informations véridiques et
vérifiées, pour vous offrir du contenu de qualité.</p>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="facts-counter">
                            <ul>
                            <a href="{{route('books-media-list-view')}}">

                                <li class="bg-light-green">
                                    <div class="fact-item">
                                        <div class="fact-icon">
                                            <i class="ebook"></i>
                                        </div>
                                        <span>Books<strong class="fact-counter">{{count_numberofbook('pdf')}}</strong></span>
                                    </div>
                                </li>

                                </a>

                                <li class="bg-green">
                                    <div class="fact-item">
                                        <div class="fact-icon">
                                            <i class="eaudio"></i>
                                        </div>
                                        <span>eAudio<strong class="fact-counter">{{count_numberofbook('eAudio')}}</strong></span>
                                    </div>
                                </li>
                                <li class="bg-red">
                                    <div class="fact-item">
                                        <div class="fact-icon">
                                            <i class="magazine"></i>
                                        </div>
                                        <span>Magazine<strong class="fact-counter">{{count_numberofbook('Magazine')}}</strong></span>
                                    </div>
                                </li>
                                <a href="{{route('video')}}">
                                <li class="bg-blue">
                                    <div class="fact-item">
                                        <div class="fact-icon">
                                            <i class="videos"></i>
                                        </div>
                                        <span>Videos<strong class="fact-counter">9</strong></span>
                                    </div>
                                </li>
                                </a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="welcome-image"></div>
        </section>
        <!-- End: Welcome Section -->
        
        <!-- Start: Category Filter -->
        <section class="category-filter section-padding">
            <div class="container">
                <div class="center-content">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                           <h2 class="section-title">TYPE DE catégorie</h2>
                            <span class="underline center"></span>
                            <p class="lead">Des millions de ressources disponibles gratuitement en ligne</p>
                        </div>
                    </div>
                </div>
                <div class="filter-buttons">
                    <div class="filter btn" data-filter="all">Les Categories</div>
                    @foreach ($categories as $category)
                        <div class="filter btn" data-filter=".{{strtolower($category->category)}}">{{$category->category}}</div>
                     @endforeach
                </div>
            </div>
            <div id="category-filter">
                <ul class="category-list">
                @foreach ($categories as $category)
                @foreach (get_category_books($category->id) as $book_item)

                <li class="category-item {{strtolower($category->category)}}">
                        <figure>
                            <img src="{{url('images/books-media/list-view/'. $book_item->img)}}" alt="{{$book_item->img}}" />
                            <figcaption class="bg-orange">
                                <div class="info-block">
                                    <a href="{{route('books-media-detail-v1',$book_item->id)}}"> 
                                    <h4>{{$book_item->title}}</h4> </a>
                                    <span class="author"><strong>Author:</strong> {{$book_item->author}}</span>
                                    <div class="rating">
                                        @for($i = 0; $i <$book_item->rating ; $i++)
                                        <span>☆</span>
                                        @endfor
                                    </div>
                                    <p>{{$book_item->description}}</p>

                                    <a href="{{route('books-media-detail-v1',$book_item->id)}}">voir plus <i class="fa fa-long-arrow-right"></i></a>
                                    <ol>
                                    
                                
                                     
                                    
                                    </ol>
                                </div>
                            </figcaption>
                        </figure>
                        @endforeach
                    </li>
                @endforeach
                </ul>
                <div class="center-content">
                    <a href="{{route('books-media-list-view')}}" class="btn btn-primary">Voir plus</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </section>
        
        @auth
        <!-- Start: Newsletter -->
        <section class="newsletter section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="center-content">
                            <h2 class="section-title">Souscrire a nos Newsletters</h2>
                            <span class="underline center"></span>
                            <p class="lead">Recevoir des informations à propos de nos nouvelles
                                électriques directement par mail</p>
                        </div>
                        <div class="form-group">
                        <form action="{{ route('newsletter') }}" method="post">

                            <input class="form-control" placeholder="Ecrire votre Email" id="newsletter" name="newsletter" type="email" required>

                            <input class="form-control" value="S'abonner" type="submit">
                            @csrf

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End: Newsletter -->
   

        @endauth

        
        <!-- Start: Our Community Section -->
        <section class="community-testimonial">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-title">Mots de notre communauté</h2>
                    <span class="underline center"></span>
                    <p class="lead">Votre avis nous intéresse</p>
                </div>
                <div class="owl-carousel">

                @foreach($data_community as $comm_data)
                    <div class="single-testimonial-box">
                        <div class="top-portion">
                            <img src="{{url('images/'.$comm_data->img)}}" alt="Testimonial Image" />
                            <div class="user-comment">
                                <div class="arrow-left"></div>
                                <blockquote cite="#">

                                {{$comm_data->content}}
                                </blockquote>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="bottom-portion">
                            <a href="#" class="author">
                            {{$comm_data->author}}

                            </a>
                       
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
       @endforeach
                </div>
            </div>
        </section>
        <!-- End: Our Community Section --> 

        
        @include('library.footerbar')

        
        @include('library.footer_inc')

        
    </body>


</html>