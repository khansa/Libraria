<!DOCTYPE html>
<html lang="fr">
    

    @include('library.header_inc')


    <body>

       @include('library.topbar')

        <!-- Start: Page Banner -->
        <section class="page-banner news-listing-banner services-banner">
            <div class="container">
                <div class="banner-header">
                    <h2>LISTE DES NOUVELLES</h2>
                    <span class="underline center"></span>
                    <p class="lead">Tous les nouveautés et les évenements dans un seul espace</p>
                </div>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="{{route('index')}}">Accueil</a></li>
                        <li>Des nouvelles
</li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- End: Page Banner -->

        <!-- Start: Products Section -->
        <div id="content" class="site-content">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="main-news-list">
                        <div class="container">
                   <br><br>
                            <div class="row">
                                <div class="col-md-9 col-md-push-3 news-events-list-view">


                                @foreach ($all_news as $news_item)
                                    <div class="news-list-box">
                                        <div class="single-news-list">
                                            <div class="social-content">
                                                <div class="social-share">
                                                    <ul>
                                                        <li><a ><i class="fa fa-eye"></i> {{$news_item->views}}</a></li>
                                                    </ul>
                                                </div>
                                             
                                            </div>
                                            <figure>
                                                <a href="{{route('news-events-detail',$news_item->id)}}"><img src="{{url('images/news-event/'.$news_item->img)}}" alt="News &amp; Event"></a>
                                            </figure>
                                            <div class="content-block">
                                                <div class="member-info">
                                                    <div class="content_meta_category">
                                                        <span class="arrow-right"></span>
                                                        <a  rel="category tag">ÉVÉNEMENT</a>
                                                    </div>
                                                    <ul class="news-event-info">
                                                        <li>
                                                            <a  target="_blank">
                                                                <i class="fa fa-calendar"></i>
                                                                {{$news_item->created_at->format('d/M/Y')}}
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a  target="_blank">
                                                                <i class="fa fa-clock-o"></i>
                                                                {{$news_item->start_time}} - {{$news_item->end_time}} 
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a  target="_blank">
                                                                <i class="fa fa-map-marker"></i>
                                                                {{$news_item->location}}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <h3><a href="{{route('news-events-detail',$news_item->id)}}">{{$news_item->title}}</a></h3>
                                                    <p>{{$news_item->content}}</p>
                                                    <a class="btn btn-primary" href="{{route('news-events-detail',$news_item->id)}}">Lire la suite</a>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                            @endforeach
                                    <nav class="navigation pagination text-center">
                                        <h2 class="screen-reader-text">Posts navigation</h2>
                                
                                    </nav>
                                </div>


                                <br>
                          
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <!-- End: Products Section -->

    

       @include('library.footerbar')

        @include('library.footer_inc')

    </body>


</html>