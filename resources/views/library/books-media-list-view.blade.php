<!DOCTYPE html>
<html lang="fr">
    

@include('library.header_inc')

    <body>

       @include('library.topbar')
                
        <!-- Start: Page Banner -->
        <section class="page-banner services-banner">
            <div class="container">
                <div class="banner-header">
                    <h2>Livres & Media</h2>
                    <span class="underline center"></span>
                    <p class="lead">Plusieurs milliers de livres sont disponibles</p>
                </div>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="{{route('index')}}">Accueil</a></li>
                        <li>Livres & Media</li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- End: Page Banner -->

        <!-- Start: Book & Media Section -->
        <div id="content" class="site-content">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="books-media-list">
                        <div class="container">
                            <div class="row">
                                <!-- Start: Search Section -->
                                <section class="search-filters">
                                    <div class="container">
                                        <div class="filter-box">
                                            <h3>Que cherchez vous?</h3>
                                            <form action="{{ route('books-media-list-view') }}" method="get">
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="sr-only" for="keywords">recherche par mots-clés</label>
                                                        <input class="form-control" placeholder="Recherche par mots-clés" id="keywords" name="keywords" type="text"


                                                        @if(isset($_GET['keywords'])) value="{{$_GET['keywords']}}">
                                                        @else value="">
                                                        @endif

                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="form-group">
                                                        <select name="category" id="category" class="form-control">
                                                            <option value ="all">Les Categories</option>

                                                            @foreach ($categories as $category)
                           <option value ="{{$category->category}}">{{$category->category}}</option>

                           @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                              
                                                <div class="col-md-2 col-sm-6">
                                                    <div class="form-group">
                                                        <input class="form-control" type="submit" value="recherche">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </section>
                                <!-- End: Search Section -->
                            </div>
                            <div class="row">
                                <div class="col-md-9 col-md-push-3">
                                    <div class="filter-options margin-list">
                                        <div class="row">   
                                        @if (sizeof($_GET) == 0 || ($_GET['category'] =='all'))


<h4 class="widget-title" data-control>Toutes les Categories</h4>
<br><br>

@else

<h4 class="widget-title" data-control>les categories:  {{$_GET['category']}}</h4>
<br><br>
@endif                                         
                                      
                                    
                                     
                                        </div>
                                    </div>
                                    <div class="books-list">
                                  
                         
                                    @if(isset($books) && count ($books)>0)

                                @foreach ($books as $book)

                                        <article> 
                                            <div class="single-book-box">
                                                <div class="post-thumbnail">
                                                    <div class="book-list-icon green-icon"></div>
                                                    <a href="{{route('books-media-detail-v1',$book->id)}}"><img alt="Book" src="{{url('images/books-media/list-view/'.$book->img)}}" /></a>
                                                </div>
                                                <div class="post-detail">
                                            
                                            
                                                    <header class="entry-header">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <h3 class="entry-title"><a href="{{route('books-media-detail-v1',$book->id)}}">{{$book->title}}</a></h3>
                                                                <ul>
                                                                    <li><strong>Auteur : </strong> {{$book->author}}</li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <ul>
                                                                    <li><strong>Edition : </strong>{{$book->edition}}</li>
                                                                    <li>
                                                                        <div class="rating">
                                                                            <strong>Évaluation : </strong>
                                                                            <span>☆</span>
                                                                            <span>☆</span>
                                                                            <span>☆</span>
                                                                            <span>☆</span>
                                                                            <span>☆</span>
                                                                        </div>
                                                                    </li>
                                                                </ul>                                                                
                                                            </div>
                                                        </div>
                                                    </header>
                                                    <div class="entry-content">
                                                        <p>{{$book->description}}</p>
                                                    </div>
                                                    <footer class="entry-footer">
                                                        <a class="btn btn-dark-gray" href="{{route('books-media-detail-v1',$book->id)}}">Accéder au détails</a>
                                                    </footer>
                                                </div>
                                            </div>
                                        </article>
                                        @endforeach

                                        @else
                                        <h3> Désolé, pas de livres  </h3>
                                        @isset($_GET['keywords'])
                                        <h3>Pas de livres pour votre recherche :

                                        {{$_GET['keywords']}} 
@endisset 
                                         </h3>

                                        @endif


                                    </div>
                                    <nav class="navigation pagination text-center">
                                        <h2 class="screen-reader-text">Posts navigation</h2>
                                    
                                    </nav>
                                </div>
                                <div class="col-md-3 col-md-pull-9">
                                    <aside id="secondary" class="sidebar widget-area" data-accordion-group>
                                        <div class="widget widget_related_search open" data-accordion>
                                        <div class="widget widget_recent_releases">
                                            <h4 class="widget-title">Type de categorie</h4>
                                            <ul>

                                            @foreach ($categories as $category)
                           <li><a href="{{route('books-media-list-view').'?category='.$category->category}}">{{$category->category}}</a></li>

                           @endforeach

                                            
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                     
                                  
                               
                                    </aside>
                                </div>
                            </div>
                        </div>
                        

                    </div>
                </main>
            </div>
        </div>
        <!-- End: Books & Media Section -->

    

       @include('library.footerbar')

        @include('library.footer_inc')

    </body>


</html>