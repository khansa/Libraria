<!DOCTYPE html>
<html lang="fr">
    

@include('library.header_inc')

    <body>

@include('library.topbar')

        <!-- Start: Page Banner -->
        <section class="page-banner services-banner">
            <div class="container">
                <div class="banner-header">
<!--                     <h2>ARTICLES DE BLOG</h2>
 -->

 <h2>ARTICLES DE BLOG
</h2>

                    <span class="underline center"></span>
                    <p class="lead">Tous les articles</p>
                </div>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="{{route('index')}}">Home</a></li>
                        <li>Blog</li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- End: Page Banner -->
        
        <!-- Start: Blog Section -->
        <div id="content" class="site-content">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="blog-main-list">
                        <div class="container">
                            <div class="row">
                                <div class="blog-page grid">
                                    @foreach ($all_blog_data as $single_blog_data)
                                    <article>
                                        <div class="grid-item blog-item">
                                            <div class="post-thumbnail">
                                                <div class="post-date-box">
                                                    <div class="post-date">
                                                        <a class="date" >{{$single_blog_data->created_at->format('d')}}</a>
                                                    </div>
                                                    <div class="post-date-month">
                                                        <a class="month" >{{$single_blog_data->created_at->format('M')}}</a>
                                                    </div>
                                                </div>
                                                <a href="{{route('blog-detail',['num'=>$single_blog_data->id])}}"><img alt="blog" src="{{url('images/blog/'.$single_blog_data->img)}}"  /></a>
                                                <div class="post-share">
                                                    <a ><i class="fa fa-comment"></i> {{$single_blog_data->comments}}</a>
                                                    <a ><i class="fa fa-eye"></i> {{$single_blog_data->views}}</a>
                                                </div>
                                            </div>
                                            <div class="post-detail">
                                                <header class="entry-header">
                                                    <div class="blog_meta_category">
                                                        <span class="arrow-right"></span>
                                                        <a  rel="category tag">{{$single_blog_data->theme}}</a>, 
                                                    </div>
                                                    <h3 class="entry-title"><a href="{{route('blog-detail',['num'=>$single_blog_data->id])}}">{{$single_blog_data->title}}</a></h3>
                                                    <div class="entry-meta">
                                                        <span><i class="fa fa-user"></i> <a >{{$single_blog_data->author}}</a></span>
                                                        <span><i class="fa fa-comment"></i> <a >{{$single_blog_data->comments}} Comments</a></span>
                                                    </div>
                                                </header>
                                                <div class="entry-content">
                                                    <p>{{substr($single_blog_data->content,0,400)}}</p>
                                                </div>
                                                <footer class="entry-footer">
                                                    <a class="btn btn-default" href="{{route('blog-detail',['num'=>$single_blog_data->id])}}">Lire la suite</a>
                                                </footer>
                                            </div>
                                        </div>
                                    </article>
                       @endforeach
                                </div>
 -->                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <!-- End: Blog Section -->

    

       @include('library.footerbar')

        @include('library.footer_inc')

    </body>


</html>