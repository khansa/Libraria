<!DOCTYPE html>
<html lang="fr">


@include('library.header_inc')


<body>

    @include('library.topbar')
    <!-- Start: Page Banner -->
    <section class="page-banner services-banner">
        <div class="container">
            <div class="banner-header">
                <h2>ARTICLES DE BLOG</h2>
                <span class="underline center"></span>
                <p class="lead">Tous les blogs sont à votre disposition</p>
            </div>
            <div class="breadcrumb">
                <ul>
                    <li><a href="{{route('index')}}">Accueil</a></li>
                    <li><a href="{{route('blog')}}">Blog</a></li>
                    <li>Détail de la publication Numéro : {{$blog_item_value->id}}</li>
                </ul>
            </div>
        </div>
    </section>
    <!-- End: Page Banner -->

    <!-- Start: Blog Section -->
    <div id="content" class="site-content">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <div class="blog-detail-main">
                    <div class="container">
                        <div class="row">
                            <div class="blog-page">
                                <div class="blog-section">
                                    <article>
                                        <div class="blog-detail">
                                            <header class="entry-header">
                                                <div class="blog_meta_category">
                                                    <a  rel="category tag">Thème : {{$blog_item_value->theme}}</a>
                                                    <!--                                                         <a href="#." rel="category tag">art</a>
 -->
                                                </div>
                                                <h2 class="entry-title">{{$blog_item_value->title}}</h2>
                                                <div class="entry-meta">
                                                    <span><i class="fa fa-user"></i> <a href="#">{{$blog_item_value->author}}</a></span>
                                                </div>
                                            </header>
                                            <div class="post-thumbnail">
                                                <div class="post-date-box">
                                                    <div class="post-date">
                                                        <a class="date" href="#.">{{$blog_item_value->created_at->format('d')}}</a>
                                                    </div>
                                                    <div class="post-date-month">
                                                        <a class="month" href="#.">{{$blog_item_value->created_at->format('M')}}</a>
                                                    </div>
                                                </div>
                                                <figure>
                                                    <img alt="blog" src="{{url('images/blog/post-detail-img.jpg')}}" />
                                                </figure>
                                            </div>
                                            <div class="post-detail">
                                                <div class="post-detail-head">
                                                    <div class="post-share">
                                                        <a href="#comments"><i class="fa fa-comment"></i> {{$comments->count() }} Comments</a>
                                                        <a><i class="fa fa-eye"></i> {{$blog_item_value->views}} Viewed</a>
                                                    </div>
                                                    <div class="post-social-share">
                                                        <div class="sharethis-inline-share-buttons post-share"></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="entry-content">
                                                    <p>

                                                        {{$blog_item_value->content}}

                                                    </p>
                                                </div>
                                         
                                            </div>
                                        </div>
                                    </article>
                                    <div class="about-author">
                                        <img src="{{url('images/blog/about-author.jpg')}}" alt="" />
                                        <div class="author-content">
                                            <div class="author-head">
                                                <h3>{{$blog_item_value->author}} </h3>
                                                <span class="underline left"></span>
                                            </div>
                                     
                                            <div class="clearfix"></div>
                                            <p>{{$blog_item_value->author_description}} </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    @if ($comments->count()>0)

                                    <div class="comments-area" id="comments">
                                        <div class="comment-bg">

                                            <h4 class="comments-title">COMMENTAIRES DES UTILISATEURS</h4>
                                            <span class="underline left"></span>
                                            <ol class="comment-list">
                                                <li class="comment even thread-even depth-1 parent">
                                                @foreach($comments as $comment)
                                                    <div class="comment-body">
                                                        <div class="comment-author vcard">
                                                            <img class="avatar avatar-32 photo avatar-default" src="{{url('images/blog/icons8-male-user-60.png')}}" alt="Comment Author">
                                                            <b class="fn">
                                                                <a class="url" rel="external nofollow" >{{$comment->author}}</a>
                                                            </b>
                                                        </div>
                                                        <footer class="comment-meta">
                                                            <div class="left-arrow"></div>
                                                        
                                                            <div class="comment-metadata">
                                                                <a >
                                                                    <time datetime="2016-01-17">
                                                                    {{$comment->created_at}}
                                                                    </time>
                                                                </a>
                                                            </div>
                                                            <div class="comment-content">
                                                                <p>{{$comment->content}}
                                                                </p>
                                                            </div>
                                                        </footer>
                                                    </div>
                                                
                                               @endforeach
                                                </li>
                                            </ol>


                                            @endif

                                        </div>
<br><br><br><br>

                                        @auth

                                        <div class="comment-respond" id="respond">
                                            <h4 class="comment-reply-title" id="reply-title">ÉCRIVEZ VOTRE COMMENTAIRE :</h4>
                                            <span class="underline left"></span>
                                            <form class="comment-form" id="commentform"  name ="commentform" method="post" action="{{route('add_blog_comment',$blog_item_value->id)}}">

                                            @csrf
                                                <div class="row">
                                                    <p class="comment-form-author input-required">
                                                        <label>
                                                            <span class="first-letter">Utilisateur : ({{Auth::user()->name }})</span>
                                                        </label>
                                                        <input name="author" id="author" type="text" disabled readonly>  
                                                    </p>
                                              
                                                    <p class="comment-form-subject input-required">
                                                        <label>
                                                            <span class="first-letter">Sujet</span>
                                                        </label>
                                                        <input name="subject" id="subject" type="text">
                                                    </p>
                                                    <p class="comment-form-comment">
                                                        <textarea  form ="commentform" name="comment" id="comment" name="comment" placeholder="Votre message"></textarea>
                                                    </p>
                                                </div>
                                                <div class="clearfix"></div>
                                                <p class="form-submit">
                                                    <input value="Envoyer" class="submit" id="submit" name="submit" type="submit">
                                                </p>
                                            </form>
                                        </div>

                                        @endauth


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
    <!-- End: Blog Section -->



    @include('library.footerbar')


    @include('library.footer_inc')
</body>


</html>