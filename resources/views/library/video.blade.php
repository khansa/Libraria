<!DOCTYPE html>
<html lang="zxx">
    

@include('library.header_inc')

    <body>

    @include('library.topbar')
                               
        <!-- Start: Page Banner -->
        <section class="page-banner services-banner">
            <div class="container">
                <div class="banner-header">
                    <h2>les Videos</h2>
                    <span class="underline center"></span>
                    <p class="lead">Du cinéma, de la musique et des spectacles,
                         des séries TV ou des dessins animés...</p>
                </div>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="{{route('index')}}">Accueil</a></li>
                        <li>Media : Video</li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- End: Page Banner -->

      <!-- Start: Products Section -->
      <!-- <div id="content" class="site-content">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="books-full-width">
                        <div class="container">
                            <!-- Start: Search Section 
                            <section class="search-filters">
                                <div class="filter-box">
                                    <h3>Que cherchez vous?</h3>
                                    <form action="http://libraria.demo.presstigers.com/index.html" method="get">
                                        <div class="col-md-4 col-sm-6">
                                            <div class="form-group">
                                                <input class="form-control" placeholder="Recherche par mots-clés" id="keywords" name="keywords" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6">
                                            <div class="form-group">
                                                <select name="catalog" id="catalog" class="form-control">
                                                    <option>Search the Catalog</option>
                                                    <option>Catalog 01</option>
                                                    <option>Catalog 02</option>
                                                    <option>Catalog 03</option>
                                                    <option>Catalog 04</option>
                                                    <option>Catalog 05</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6">
                                            <div class="form-group">
                                                <select name="category" id="category" class="form-control">
                                                    <option>Toutes Catégories</option>
                                                    <option>Category 01</option>
                                                    <option>Category 02</option>
                                                    <option>Category 03</option>
                                                    <option>Category 04</option>
                                                    <option>Category 05</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="form-group">
                                                <input class="form-control" type="submit" value="Search">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="clear"></div>
                            </section> -->
                            <!-- End: Search Section -->
                            
                        <!--    <div class="filter-options margin-list">
                                <div class="row">                                            
                                    <div class="col-md-3 col-sm-3">
                                        <select name="orderby">
                                            <option selected="selected">Sort by Title</option>
                                            <option>Sort by popularity</option>
                                            <option>Sort by rating</option>
                                            <option>Sort by newness</option>
                                            <option>Sort by price</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <select name="orderby">
                                            <option selected="selected">Sort by Author</option>
                                            <option>Sort by popularity</option>
                                            <option>Sort by rating</option>
                                            <option>Sort by newness</option>
                                            <option>Sort by price</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2 col-sm-3">
                                        <select name="orderby">
                                            <option selected="selected">Language</option>
                                            <option>Sort by popularity</option>
                                            <option>Sort by rating</option>
                                            <option>Sort by newness</option>
                                            <option>Sort by price</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2 col-sm-3">
                                        <select name="orderby">
                                            <option selected="selected">Publishing Date</option>
                                            <option>Sort by popularity</option>
                                            <option>Sort by rating</option>
                                            <option>Sort by newness</option>
                                            <option>Sort by price</option>
                                        </select>
                                    </div>    -->
                                    <!--<div class="col-md-2 col-sm-12 pull-right">
                                        <div class="filter-toggle">
                                            <a href="books-media-gird-view-v2.html" class="active"><i class="glyphicon glyphicon-th-large"></i></a>
                                            <a href="books-media-list-view.html"><i class="glyphicon glyphicon-th-list"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            <div class="booksmedia-fullwidth">
                                <ul>
                                    <li>
                                        <div class="book-list-icon blue-icon"></div>
                                        <figure>
                                            <a href="https://www.france.tv/france-4/goldorak/"><img src="images/books-media/layout-3/books-media-layout3-01.jpg" alt="video"></a>
                                            <figcaption>
                                                <header>
                                                    <h4><a href="https://www.france.tv/france-4/goldorak/">Goldorak</a></h4>
                                                    <p>Série animée</p>
                                    
                                                </header>
                                                <p>Moment nostalgie pour les plus grands, et instant de découverte pour les plus jeunes</p>
                                    
                                            </figcaption>
                                        </figure>                                                
                                    </li>
                                    <li>
                                        <div class="book-list-icon blue-icon"></div>
                                        <figure>
                                            <a href="https://www.france.tv/slash/lastman/"><img src="images/books-media/layout-3/books-media-layout3-02.jpg" alt="video"></a>
                                            <figcaption>
                                                <header>
                                                    <h4><a href="https://www.france.tv/slash/lastman/">Lastman</a></h4>
                                                    <p>Série animée</p>
                                                    
                                                </header>
                                                <p>Vous aimez le manga Lastman ? Précipitez-vous sur la série animée, préquelle de la BD.</p>
                                                
                                            </figcaption>
                                        </figure>                                                
                                    </li>
                                    <li>
                                        <div class="book-list-icon blue-icon"></div>
                                        <figure>
                                            <a href="https://www.francemusique.fr/concerts"><img src="images/books-media/layout-3/books-media-layout3-03.jpg" alt="video"></a>
                                            <figcaption>
                                                <header>
                                                    <h4><a href="https://www.francemusique.fr/concerts">France musique : les concerts</a></h4>
                                                    <p>Concerts</p>
                                                    
                                                </header>
                                                <p>France Musique propose des centaines de concerts filmés sur son site web 
                                                    en partenariat avec Arte. Du classique bien sûr avec de la musique symphonique 
                                                    ou de chambre, mais aussi du jazz interprétés notamment par l'Orchestre 
                                                    philharmonique de Radio France ou l'Orchestre National de France.</p>
                                                
                                            </figcaption>
                                        </figure>                                                
                                    </li>
                                    <li>
                                        <div class="book-list-icon blue-icon"></div>
                                        <figure>
                                            <a href="https://www.mk2curiosity.com/"><img src="images/books-media/layout-3/books-media-layout3-04.jpg" alt="video"></a>
                                            <figcaption>
                                                <header>
                                                    <h4><a href="https://www.mk2curiosity.com/">MK2 curiosity</a></h4>
                                                    <p>Cinéma</p>
                                                    
                                                </header>
                                                <p>Chaque jeudi, la plateforme VoD du groupe MK2 propose des films à voir gratuitement pendant 7 jours.</p>
                                               
                                            </figcaption>
                                        </figure>                                                
                                    </li>
                                    <li>
                                        <div class="book-list-icon blue-icon"></div>
                                        <figure>
                                            <a href="http://font-de-gaume.monuments-nationaux.fr/fr"><img src="images/books-media/layout-3/books-media-layout3-05.jpg" alt="Book"></a>
                                            <figcaption>
                                                <header>
                                                    <h4><a href="http://font-de-gaume.monuments-nationaux.fr/fr">The Great Gatsby</a></h4>
                                                    <p>La grotte de Font-de-Gaume est l'une des plus célèbres grottes ornées de la Dordogne.</p>
                                                    
                                                <p>Visite virtuelle de la célèbre grotte préhistorique </p>
                                                
                                            </figcaption>
                                        </figure>                                                
                                    </li>
                                    <li>
                                        <div class="book-list-icon blue-icon"></div>
                                        <figure>
                                            <a href="https://www.arte.tv/fr/videos/RC-015451/courts-metrages/"><img src="images/books-media/layout-3/books-media-layout3-06.jpg" alt="Book"></a>
                                            <figcaption>
                                                <header>
                                                    <h4><a href="https://www.arte.tv/fr/videos/RC-015451/courts-metrages/">Court-circuit</a></h4>
                                                    <p> Court-métrage</p>
                                                   
                                                </header>
                                                <p>Chaque semaine, la magazine d'Arte vous propose des courts du monde entier 
                                                    (fictions récentes, films d’animation) mais aussi des portraits de réalisateurs,
                                                     des making of, des zooms sur les écoles de cinéma, sur des festivals, 
                                                     des leçons d'animation mais aussi les "trucs" du cinéma. </p>
                                                
                                            </figcaption>
                                        </figure>                                                
                                    </li>
                                    <li>
                                        <div class="book-list-icon blue-icon"></div>
                                        <figure>
                                            <a href="https://www.arte.tv/fr/videos/095460-000-A/les-espionnes-racontent-bande-annonce/"><img src="images/books-media/layout-3/books-media-layout3-07.jpg" alt="video"></a>
                                            <figcaption>
                                                <header>
                                                    <h4><a href="https://www.arte.tv/fr/videos/095460-000-A/les-espionnes-racontent-bande-annonce/">Les espionnes racontent</a></h4>
                                                    <p>Web série</p>
                                                    
                                                </header>
                                                <p>Des États-Unis à l’URSS en passant par Israël, Les Espionnes racontent, 
                                                    palpitante websérie documentaire animée que la journaliste Chloé Aeberhardt
                                                     a adaptée de son livre éponyme avec la réalisatrice Aurélie Pollet, </p>
                                                
                                            </figcaption>
                                        </figure>                                                
                                    </li>
                                    <li>
                                        <div class="book-list-icon blue-icon"></div>
                                        <figure>
                                            <a href="https://www.youtube.com/channel/UC32vOdZp-NN4eZZhJrUNR6w"><img src="images/books-media/layout-3/books-media-layout3-08.jpg" alt="Book"></a>
                                            <figcaption>
                                                <header>
                                                    <h4><a href="https://www.youtube.com/channel/UC32vOdZp-NN4eZZhJrUNR6w">Les boloss des belles lettres</a></h4>
                                                    <p>Littérature</p>
                                                    
                                                </header>
                                                <p>Initialement diffusée sur France 5, cette série (elle-même tirée d'un ouvrage de Quentin Leclerc et Michel Pimpant)
                                                      propose de revisiter les oeuvres du patrimoine littéraire classique en
                                                       les présentant de façon décomplexée et enthousiaste</p>
                                                
                                            </figcaption>
                                        </figure>                                                
                                    </li>
                                    <li>
                                        <div class="book-list-icon blue-icon"></div>
                                        <figure>
                                            <a href="https://www.france.tv/slash/invisibles/"><img src="images/books-media/layout-3/books-media-layout3-09.jpg" alt="Book"></a>
                                            <figcaption>
                                                <header>
                                                    <h4><a href="https://www.france.tv/slash/invisibles/">Invisibles</a></h4>
                                                    <p>Web série</p>
                                                    
                                                </header>
                                                <p>Réalisé par les créateurs de Data Gueule "Les invisibles" est une série documentaire qui part à la 
                                                    rencontre des forçats du numérique, ceux qu’on ne voit 
                                                    plus ou dont on ignore même souvent l’existence.</p>
                                                
                                            </figcaption>
                                        </figure>                                                
                                    </li>
                                </ul>
                            </div>
                       
                        </div>
                       
                    </div>
                </main>
            </div>
        </div>
        <!-- End: Products Section -->

 

        
        @include('library.footerbar')
        @include('library.footer_inc')
    </body>


</html>