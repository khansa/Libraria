    <!-- Start: Footer -->
    <footer class="site-footer">
            <div class="container">
                <div id="footer-widgets">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 widget-container">
                            <div id="text-2" class="widget widget_text">
                                <h3 class="footer-widget-title">A propos de Libraria</h3>
                                <span class="underline left"></span>
                                <div class="textwidget">
                                Une librairie virtuelle contenant toutes les ressources culturelles afin de faciliter l'accès à l’information et à la documentation.
                                </div>
                                <address>
                                    <div class="info">
                                        <i class="fa fa-location-arrow"></i>
                                        <span>13 Rue Ibn Nadim ,Montplaisir, Tunis Cité Monplaisir, 1073</span>
                                    </div>
                                    <div class="info">
                                        <i class="fa fa-envelope"></i>
                                        <span><a href="mailto:assistance@uvt.rnu.tn">assistance@uvt.rnu.tn</a></span>
                                    </div>
                                    <div class="info">
                                        <i class="fa fa-phone"></i>
                                        <span><a href="tel:012-345-6789">+216 71905248</a></span>
                                    </div>
                                </address>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-6 widget-container">
                            <div id="nav_menu-2" class="widget widget_nav_menu">
                                <h3 class="footer-widget-title">Liens utiles</h3>
                                <span class="underline left"></span>
                                <div class="menu-quick-links-container">
                                    <ul id="menu-quick-links" class="menu">
                                        <li><a href="{{route('news-events-list-view')}}">Nouveautés Library</a></li>
                                        <li><a href="https://www.uvt.rnu.tn">Site Web UVT</a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix hidden-lg hidden-md hidden-xs tablet-margin-bottom"></div>
                 


         



                    </div>
                </div>                
            </div>
            <div class="sub-footer">
                <div class="container">
                    <div class="row">
                        <div class="footer-text col-md-3">
                        </div>
                        <div class="col-md-9 pull-right">
                            <ul>
                                <li><a href="{{route('index')}}">Accueil</a></li>
                                <li><a href="{{route('books-media-list-view')}}">Livres &amp; Media</a></li>
                                <li><a href="{{route('news-events-list-view')}}">Nouveauté &amp; Évènement</a></li>
                                <li><a href="{{route('services')}}">Services</a></li>
                                <li><a href="{{route('blog')}}">Blog</a></li>
                                <li><a href="{{route('contact')}}">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End: Footer -->