<!DOCTYPE html>
<html lang="fr">
    

@include('library.header_inc')



    <body>

       @include('library.topbar')

        <!-- Start: Page Banner -->
        <section class="page-banner services-banner">
            <div class="container">
                <div class="banner-header">
                    <h2>Contactez-nous</h2>
                    <span class="underline center"></span>
                    <p class="lead"></p>
                </div>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="{{route('index')}}">Accueil</a></li>
                        <li>Contact</li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- End: Page Banner -->
        
        <!-- Start: Contact Us Section -->
        <div id="content" class="site-content">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="contact-main">
                        <div class="contact-us">
                            <div class="container">
                                <div class="contact-location">
                                    <div class="flipcard">
                                        <div class="front">
                                            <div class="top-info">
                                                <span><i class="fa fa-map-marker" aria-hidden="true"></i> Notre Adresse</span>
                                            </div>
                                            <div class="bottom-info">
                                                <span class="top-arrow"></span>
                                                <ul>
                                                    <li>13 Rue Ibn Nadim </li>
                                                    <li>Montplaisir</li>
                                                    <li>Tunis Cité Monplaisir, 1073</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="back">
                                            <div class="bottom-info orange-bg">
                                                <span class="bottom-arrow"></span>
                                                <ul>
                                                    <li>13 Rue Ibn Nadim </li>
                                                    <li>Montplaisir</li>
                                                    <li>Tunis Cité Monplaisir, 1073</li>
                                                </ul>
                                            </div>
                                            <div class="top-info dark-bg">
                                                <span><i class="fa fa-map-marker" aria-hidden="true"></i> Notre Adresse</span>
                                            </div>                                                
                                        </div>
                                    </div>
                                    <div class="flipcard">
                                        <div class="front">
                                            <div class="top-info">
                                                <span><i class="fa fa-fax" aria-hidden="true"></i> Téléphone et Fax</span>
                                            </div>
                                            <div class="bottom-info">
                                                <span class="top-arrow"></span>
                                                <ul>
                                                    <li><a >Local: +216 71 90 52 48</a></li>
                                                    <li><a >Local: +216 71 90 52 69</a></li>
                                                    <li><a >Fax: +216 71903603</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="back">
                                            <div class="bottom-info orange-bg">
                                                <span class="bottom-arrow"></span>
                                                <ul>
                                                    <li><a >Local: +216 71 90 52 48</a></li>
                                                    <li><a >Local: +216 71 90 52 69</a></li>
                                                    <li><a >Fax: +216 71903603</a></li>
                                                </ul>
                                            </div>
                                            <div class="top-info dark-bg">
                                                <span><i class="fa fa-fax" aria-hidden="true"></i> Téléphone et Fax</span>
                                            </div>                                                
                                        </div>
                                    </div>
                                    <div class="flipcard">
                                        <div class="front">
                                            <div class="top-info">
                                                <span><i class="fa fa-envelope" aria-hidden="true"></i>Adresse Email</span>
                                            </div>
                                            <div class="bottom-info">
                                                <span class="top-arrow"></span>
                                                <ul>
                                                    <li>www.libraria.com</li>
                                                    <li>assistance@uvt.rnu.tn</li>
                                                    <li>info@libraria.com</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="back">
                                            <div class="bottom-info orange-bg">
                                                <span class="bottom-arrow"></span>
                                                <ul>
                                                    <li><a href="http://www.libraria.com/">www.libraria.com </a></li>
                                                    <li><a href="mailto:assistance@uvt.rnu.tn">assistance@uvt.rnu.tn</a></li>
                                                    <li><a href="mailto:info@libraria.com">info@libraria.com</a></li>
                                                </ul>
                                            </div>
                                            <div class="top-info dark-bg">
                                                <span><i class="fa fa-envelope" aria-hidden="true"></i> Adresse Email</span>
                                            </div>                                                
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="row">
                                    <div class="contact-area">
                                        <div class="container">
                                            <div class="col-md-5 col-md-offset-1 border-gray-left">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="contact-map bg-light margin-left">
                                                            <div class="company-map" id="map"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5 border-gray-right">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="contact-form bg-light margin-right">
                                                            <h2>Envoyez-nous un message</h2>
                                                            <span class="underline left"></span>
                                                            <div class="contact-fields">
                                                                <form id="the_contact" name="contact" action="{{route('contact')}}" method="post" >
                                                                    <div class="row">

                                                                        <div class="col-md-6 col-sm-6">
                                                                            <div class="form-group">
                                                                                <input class="form-control" type="text" placeholder="Prénom" name="first_name" id="first_name" size="30" value="" aria-required="true" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <div class="form-group">
                                                                                <input class="form-control" type="text" placeholder="Nom" name="last_name" id="last_name" size="30" value="" aria-required="true" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <div class="form-group">
                                                                                <input class="form-control" type="email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" placeholder="Email" name="email" id="email" size="30" value="" aria-required="true" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6">
                                                                            <div class="form-group">
                                                                                <input class="form-control" type="tel" placeholder="Numéro de téléphone" name="phone" id="phone" size="30" value="" aria-required="true" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <textarea  form="the_contact"  name ="message"  class="form-control" placeholder="Votre message" id="message" aria-required="true"></textarea>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>

                                                                        @csrf

                                                                        <div class="col-sm-12">
                                                                            <div class="form-group form-submit">
                                                                                <input class="btn btn-default" id="submit-contact-form" type="submit" name="submit" value="Envoyer"  />
                                                                            </div>
                                                                        </div>
                                                                        <div id="success">
                                                                            <span>Votre message a ete bien envoyé!nous reviendrons vers vous dans les meilleurs délais</span>
                                                                        </div>

                                                                        <div id="error">
                                                                            <span>Il y a un probleme, SVP essayer de faire n refresh et renvoyer le formlaire encore une fois.</span>
                                                                        </div>
                                                                    </div>
                                                                </form> 
                                                            </div>                                                                   
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <!-- End: Contact Us Section -->

    

       @include('library.footerbar')

        @include('library.footer_inc')

    </body>


</html>
