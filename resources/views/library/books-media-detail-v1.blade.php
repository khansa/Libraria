<!DOCTYPE html>
<html lang="fr">
    
    @include('library.header_inc')


    <body>
       @include('library.topbar')

        <!-- Start: Page Banner -->
        <section class="page-banner services-banner">
            <div class="container">
                <div class="banner-header">
                    <h2>LIVRES ET LISTE DES MÉDIAS</h2>
                    <span class="underline center"></span>
                    <p class="lead">Plusieurs milliers de livres sont disponibles</p>
                </div>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="{{route('index')}}">Accueil</a></li>
                        <li>
                        Livres et médias


                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- End: Page Banner -->

        <!-- Start: Products Section -->
        <div id="content" class="site-content">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="booksmedia-detail-main">
                        <div class="container">
                            <div class="row">
                                <!-- Start: Search Section -->
                                <section class="search-filters">
                                    <div class="container">
                                        <div class="filter-box">
                                            <h3>Que cherchez vous?</h3>
                                            <form action="{{ route('books-media-list-view') }}" method="get">
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="sr-only" for="keywords">Recherche par mots-clés</label>
                                                        <input class="form-control" placeholder="Recherche par mots-clés" id="keywords" name="keywords" type="text"


                                                        @if(isset($_GET['keywords']))
value="{{$_GET['keywords']}}">
@else
value="">
@endif





                                                    </div>
                                                </div>
                                    
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="form-group">
                                                        <select name="category" id="category" class="form-control">
                                                            <option value ="all">Toutes Catégories</option>
                                                    
                                                            @foreach ($categories as $category)
                           <option>{{$category->category}}</option>

                           @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-sm-6">
                                                    <div class="form-group">
                                                        <input class="form-control" type="submit" value="Recherche">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </section>
                                <!-- End: Search Section -->

                                <br><br><br><br><br>
                            </div>
                            <div class="row">
                                <div class="col-md-9 col-md-push-3">
                                    <div class="booksmedia-detail-box">
                                        <div class="single-book-box">                                                
                                            <div class="post-thumbnail">
                                                <div class="book-list-icon yellow-icon"></div>
                                                <img alt="Book" src="{{url('images/books-media/list-view/'.$book_item_value->img)}}" />                                                    
                                            </div>
                                            <div class="post-detail">
                                           
                                          
                                                <header class="entry-header">
                                                    <h2 class="entry-title">{{$book_item_value->title}}</h2>
                                                    <ul>
                                                        <li><strong>Auteur :</strong> {{$book_item_value->author}}</li>
                                                        <li>
                                                            <div class="rating">
                                                                <strong>Rating :</strong> 
                                                                @for  ($i = 0; $i <$book_item_value->rating ; $i++)

<span>☆</span>


@endfor                                                          
                                                            </div>
                                                        </li>
                                                        <li><strong>Edition :</strong> {{$book_item_value->edition}}</li>
                                                        <li><strong>Publisher :</strong> {{$book_item_value->publisher}}</li>
                                                    </ul>
                                                </header>

@if (check_if_file_exist("$book_item_value->id.$book_item_value->format") && Auth::check())

                                                <div class="entry-content post-buttons">
                                                    <a href="{{route('showbook',['id' => "$book_item_value->id.$book_item_value->format"])}}" class="btn btn-dark-gray">Visualiser</a>



                                                    <a href="{{route('downloadbook',['id' => "$book_item_value->id.$book_item_value->format",'title' => $book_item_value->title])}}" class="btn btn-dark-gray">Télécharger ({{get_size_file_book( "$book_item_value->id.$book_item_value->format")}})</a>



                                                </div>

@elseif (Auth::check())

<a  class="btn btn-dark-gray" disabled> Conetenu indisponible</a>


@elseif (!Auth::check())

<br><br>
<h2 style="color:gray">Veuillez se connecter</h2>
<br><br>
<a  href="{{route('register')}}" class="btn btn-dark-gray" > Inscription</a>
<a href="{{route('login')}}" class="btn btn-dark-gray" > Connexion</a>

                                                @endif







                                            </div>
                                        </div>
                                        <p><strong>Déscription : </strong> {{$book_item_value->description}} </p>
                                        <ul class="detail-page-listing">
                                            <li><strong>Length : </strong> {{$book_item_value->length}} pages</li>
                                            <li><strong>Format : </strong> {{$book_item_value->format}}</li>
                                            <li><strong>Langue : </strong> {{$book_item_value->language}}</li>

                                        </ul>
                                  
                                     
                                    </div>
                                </div>
                                <div class="col-md-3 col-md-pull-9">
                                    <aside id="secondary" class="sidebar widget-area" data-accordion-group>
                                  
                                        <div class="widget widget_recent_releases">
                                            <h4 class="widget-title">Type of category</h4>
                                            <ul>
                                            @foreach ($categories as $category)
                           <li><a href="{{route('books-media-list-view').'?category='.$category->category}}">{{$category->category}}</a></li>

                           @endforeach
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                      
                                    </aside>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <!-- End: Products Section -->

    

       @include('library.footerbar')

       @include('library.footer_inc')
    </body>


</html>