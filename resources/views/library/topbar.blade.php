        <!-- Start: Header Section -->
        <header id="header-v1" class="navbar-wrapper">
            <div class="container">
                <div class="row">
                    <nav class="navbar navbar-default">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="navbar-header">
                                    <div class="navbar-brand">
                                        <h1>
                                            <a href="{{route('index')}}">
                                                <img src="{{url('images/libraria-logo-v1.png')}}" alt="LIBRARIA" />
                                            </a>
                                        </h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <!-- Header Topbar -->
                                <div class="header-topbar hidden-sm hidden-xs">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="topbar-info">
                                                <a href="tel:+216 71905248 "><i class="fa fa-phone"></i>+216 71905248 </a>
                                                <span>/</span>
                                                <a href="mailto:assistance@uvt.rnu.tn"><i class="fa fa-envelope"></i>assistance@uvt.rnu.tn</a>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="topbar-links">
                                            @auth
                                            <a href="{{route('dashboard')}}"><i class="fa fa-lock"></i>{{Auth::user()->name}}</a>
                                            <a href="{{route('disconnect')}}" class="deconnect">Déconnecter</a>


                                            @endauth

                                            @guest
                                            <a href="{{route('login')}}"><i class="fa fa-lock"></i>Se connecter/</a>
                                            <a href="{{route('register')}}">Inscription</a>
                                            @endguest
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="navbar-collapse hidden-sm hidden-xs">
                                    <ul class="nav navbar-nav">
                                        <li class="dropdown active">
                                            <a data-toggle="dropdown" class="dropdown-toggle disabled" href="{{route('index')}}">Accueil</a>
                                 
                                        </li>
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle disabled" href="{{route('books-media-list-view')}}">Livres &amp; Media</a>
                         
                                        </li>
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle disabled" href="{{route('news-events-list-view')}}">Nouveauté &amp; Évènement</a>
                             
                                        </li>
                               
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle disabled" href="{{route('blog')}}">Blog</a>
                                   
                                        </li>
                                        <li><a href="{{route('services')}}">Services</a></li>
                                        <li><a href="{{route('contact')}}">Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="mobile-menu hidden-lg hidden-md">
                            <a href="#mobile-menu"><i class="fa fa-navicon"></i></a>
                            <div id="mobile-menu">
                                <ul>
                                    <li class="mobile-title">
                                        <h4>Navigation</h4>
                                        <a href="#" class="close"></a>
                                    </li>
                               
                                    <li>
                                        <a href="{{route('books-media-list-view')}}">Livres &amp; Media</a>
                                
                                    </li>
                                    <li>
                                        <a href="{{route('news-events-list-view')}}">Nouveauté &amp; Évènement</a>
                                  
                                    </li>
                               
                                    <li>
                                        <a href="{{route('blog')}}">Blog</a>
                                        <ul>
                            
                                        </ul>
                                    </li>
                                    <li><a href="{{route('services')}}">Services</a></li>
                                    <li><a href="{{route('contact')}}">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        <!-- End: Header Section -->